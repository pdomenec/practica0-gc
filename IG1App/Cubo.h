#pragma once
#include "EntityWithIndexMesh.h"
#include <vector>

class Cubo : public EntityWithIndexMesh
{
protected:
	glm::dvec3 color;
public:

	Cubo(GLdouble l, glm::dvec4 color);

	void setColor(glm::dvec3 c) { color = c; }

	void render(glm::dmat4 const& modelViewMat) const;
	void setCopper() const;
};

