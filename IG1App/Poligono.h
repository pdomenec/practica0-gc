/*
 * File:   Poligono.h
 * Author: Javier L�pez Quesada y Pedro Pablo Dom�nech Arellano
 */
#ifndef _H_Poligono_H_
#define _H_Poligono_H_

#include "Entity.h"
class Poligono : public Abs_Entity
{
public:
	explicit Poligono(GLuint, GLdouble);
	virtual void render(glm::dmat4 const& modelViewMat) const;
};

#endif // _H_Poligono_H_