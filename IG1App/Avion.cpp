#include "Avion.h"

Avion::Avion()
{
	glm::dvec4 verde = { 0, 1, 0, 1 };
	this->chasis = new Chasis();
	this->alas = new Cubo(350, verde);
	this->focoVientre = new SpotLight({ 0, 850, 0 });
	this->focoVientre->setSpot({ 0, -1, 0 }, 45.0, 4.0);
	this->focoVientre->setDiff({ 1, 1, 1, 1 });


	this->alas->setModelMat(
		glm::scale(this->alas->modelMat(), glm::dvec3(1, 0.06, 0.19))
	);

	this->gObjects.push_back(this->chasis);
	this->gObjects.push_back(this->alas);

	angleRot = glm::radians(0.0);
	angleRotFoco = glm::radians(135.0);
	angleTrans = glm::radians(135.0);
	mTrans = glm::translate(
		glm::dmat4(1.0),
		glm::dvec3(
			0.0,
			-250.0 * (glm::cos(angleTrans) - glm::sin(angleTrans)),
			-250.0 * (glm::cos(angleTrans) + glm::sin(angleTrans))
		)
	);
	this->entityType = "Avion";
}

void Avion::render(glm::dmat4 const& modelViewMat) const
{
	glm::dmat4 aMat = modelViewMat * mTrans * mModelMat;  // glm matrix multiplication
	upload(aMat);
	for (Abs_Entity* el : gObjects) {
		el->render(aMat);
	}
}

void Avion::encenderFoco(glm::dmat4 const& modelViewMat)
{
	this->focoVientre->enable();
	this->focoVientre->upload(modelViewMat*mModelMat);
}

void Avion::apagarFoco()
{
	this->focoVientre->disable();
}

void Avion::update(glm::dmat4 const& modelViewMat)
{
	glm::dvec3 translacion = glm::dvec3(
		0.0,
		-250.0 * (glm::cos(angleTrans) - glm::sin(angleTrans)),
		-250.0 * (glm::cos(angleTrans) + glm::sin(angleTrans))
	);
	glm::dvec3 translacionFoco = glm::dvec3(
		0.0,
		-600.0 * (glm::cos(angleTrans) - glm::sin(angleTrans)),
		-600.0 * (glm::cos(angleTrans) + glm::sin(angleTrans))
	);
	angleTrans += glm::radians(5.0);

	glm::dmat4 newmTrans = glm::translate(glm::dmat4(1.0), translacion);
	this->focoVientre->setPosDir(translacionFoco);
	
	newmTrans = glm::rotate(newmTrans, angleRot, glm::dvec3(-1, 0, 0));
	glm::fvec3 direccionFoco = this->focoVientre->getDirection();
	this->focoVientre->setDirection({
		0.0,
		1.0 * (glm::cos(angleRotFoco) - glm::sin(angleRotFoco)),
		1.0 * (glm::cos(angleRotFoco) + glm::sin(angleRotFoco))
	});
	angleRot -= glm::radians(5.0);
	angleRotFoco += glm::radians(5.0);

	mTrans = newmTrans;
	chasis->update(modelViewMat);

	render(modelViewMat);
}
