/*
 * File:   CajaTex.h
 * Author: Javier L�pez Quesada y Pedro Pablo Dom�nech Arellano
 */
#ifndef _H_CajaTex_H_
#define _H_CajaTex_H_

#include "Entity.h"

/**
 * @class CajaTex
 * @brief Clase encargada de la creaci�n de una caja con doble textura
 */
class CajaTex : public Abs_Entity
{
protected:
	GLdouble ld;         // Lado de la caja
	glm::dmat4 mTrans;   // Matriz de transformaci�n
	Mesh* meshFondo;     // Malla encargada del fondo, se le asignar� un cuadrado
	bool renderFondo;
	Texture* texturaInt; // Textura interior de la caja
	Texture* texturaExt;
public:

	virtual ~CajaTex() {
		delete mMesh;
		mMesh = nullptr;
		delete meshFondo;
		meshFondo = nullptr;
	};

	explicit CajaTex(GLdouble _ld, Texture* _texturaExt, Texture* _texturaInt);
	virtual void render(glm::dmat4 const& modelViewMat) const;

	void setTexturaInt(std::string bpm_name, GLubyte alpha) {
		texturaInt->load(bpm_name, alpha);
	}

	void setTexturaExt(std::string bpm_name, GLubyte alpha) {
		mTexture->load(bpm_name, alpha);
	}

	void setRenderFondo(bool r) {
		renderFondo = r;
	}
};

#endif // _H_Caja_H_
