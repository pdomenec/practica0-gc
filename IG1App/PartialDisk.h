#pragma once
#include "QuadricEntity.h"
class PartialDisk : public QuadricEntity
{
public:
	PartialDisk(GLdouble iRad, GLdouble oRad, GLdouble startAng, GLdouble sweepAng);
	void render(glm::dmat4 const& modelViewMat) const;
protected:
	GLdouble innerRadius;
	GLdouble outterRadius;
	GLdouble startAngle;
	GLdouble sweepAngle;
};
