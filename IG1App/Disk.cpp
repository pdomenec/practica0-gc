#include <gtc/matrix_transform.hpp>  
#include <gtc/type_ptr.hpp>

#include "Disk.h"


Disk::Disk(GLdouble iRad, GLdouble oRad)
{
	innerRadius = iRad;
	outterRadius = oRad;
}

void Disk::render(glm::dmat4 const& modelViewMat) const {
	glm::dmat4 aMat = modelViewMat * mModelMat;
	upload(aMat);
	// Aqu� se puede fijar el color de la esfera as�:
	glEnable(GL_COLOR_MATERIAL);
	glColor3f(color.r, color.g, color.b);
	// Aqu� se puede fijar el modo de dibujar la esfera:
	gluQuadricDrawStyle(q, GLU_FILL);
	gluDisk(q, innerRadius, outterRadius, 50, 50);
	// Aqu� se debe recuperar el color:
	glColor3f(1.0, 1.0, 1.0);
	glDisable(GL_COLOR_MATERIAL);
}