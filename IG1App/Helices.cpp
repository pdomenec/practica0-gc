#include "Helices.h"
#include <gtc/matrix_transform.hpp>  
#include <gtc/type_ptr.hpp>

Helices::Helices()
{
	this->helice1 = new Cylinder(20, 5, 40);
	this->helice2 = new Cylinder(20, 5, 40);

	this->helice1->setColor({ 0, 0, 1 });
	this->helice2->setColor({ 0, 0, 1 });
	this->helice2->setModelMat(
		glm::rotate(this->helice2->modelMat(), glm::radians(180.0), glm::dvec3(0, 1, 0))
	);

	mTrans = glm::dmat4(1.0);
	angleRot = glm::radians(10.0);

	gObjects.push_back(this->helice1);
	gObjects.push_back(this->helice2);
}

void Helices::update(glm::dmat4 const& modelViewMat) {

	glm::dmat4 giroX = glm::rotate(glm::dmat4(1.0), angleRot, glm::dvec3(1, 0, 0));
	angleRot -= glm::radians(15.0);

	mTrans = giroX;;
}

void Helices::render(glm::dmat4 const& modelViewMat) const
{
	glm::dmat4 aMat = modelViewMat  * mModelMat * mTrans;  // glm matrix multiplication
	upload(aMat);
	for (Abs_Entity* el : gObjects) {
		el->render(aMat);
	}
}
