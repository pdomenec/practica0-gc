#pragma once
#include "EntityWithIndexMesh.h"
#include "MbR.h"
#include <vector>

class Cono : public EntityWithIndexMesh
{
protected:
	glm::dvec3 color = { 0, 0, 1 };
public:
	explicit Cono(GLdouble h, GLdouble r, GLint n);
	void generateVColors();
	void render(glm::dmat4 const& modelViewMat) const;
};

