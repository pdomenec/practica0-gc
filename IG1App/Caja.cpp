/*
 * File:   Caja.cpp
 * Author: Javier L�pez Quesada y Pedro Pablo Dom�nech Arellano
 */
#include <gtc/matrix_transform.hpp>  
#include <gtc/type_ptr.hpp>

#include "Caja.h"

 /**
  * @brief Constructor parametrizado de caja
  * @param ld lado de la caja
  */
Caja::Caja(GLdouble ld) {
	this->ld = ld;
	mMesh = Mesh::generaContCubo(ld);           // Se genera el cubo para los lados
	meshFondo = Mesh::generaRectangulo(ld, ld); // Se genera un cuadrado para el fondo
	mTrans = glm::dmat4(1.0);
	entityType = "Caja";
}

/**
  * @brief Transfiere la matriz de modelado a la GPU
  * @param modelViewMat matriz de modelado
  */
void Caja::render(glm::dmat4 const& modelViewMat) const {
	if (mMesh != nullptr) {
		glm::dmat4 aMat = modelViewMat * mTrans * mModelMat;  // glm matrix multiplication

		glm::dmat4 mTransSuelo = glm::rotate(glm::dmat4(1.0), glm::radians(90.0), glm::dvec3(1, 0, 0)); // Rotamos el fondo 90 grados
		mTransSuelo = glm::translate(mTransSuelo, glm::dvec3(0, 0, ld / 2.0));                          // Lo trasladamos para que est� en y=0

		glm::dmat4 aMatSuelo = modelViewMat * mTrans * mModelMat * mTransSuelo;  // glm matrix multiplication

		// Las l�neas se ver�n por delante y por detr�s
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		upload(aMat);
		mMesh->render();      // Se renderizan los lados
		upload(aMatSuelo);
		meshFondo->render();  // Se renderiza el fondo
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
}
