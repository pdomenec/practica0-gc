#pragma once
#include "PosLight.h"
class SpotLight : public PosLight {
protected:
    // Atributos del foco
    glm::fvec4 direction = { 0, 0, -1, 0 };
    GLfloat cutoff = 180;
    GLfloat exp = 0;
public:
    SpotLight(glm::fvec3 pos = { 0, 0, 0 })
        : PosLight() {
        posDir = glm::fvec4(pos, 1.0);
    };
    ~SpotLight() {
        disable();
        cont--;
    }
    virtual void upload(glm::dmat4 const& modelViewMat) const;
    void setSpot(glm::fvec3 dir, GLfloat cf, GLfloat e);
    void setDirection(glm::fvec3 dir) { direction = { dir, 0 }; };
    glm::fvec3 getDirection() { return { direction.x, direction.y, direction.z }; };

};
