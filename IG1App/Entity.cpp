/*
 * File:   Entity.cpp
 * Author: Javier López Quesada y Pedro Pablo Doménech Arellano
 */
#include "Entity.h"

#include <gtc/matrix_transform.hpp>  
#include <gtc/type_ptr.hpp>

using namespace glm;

/**
 * @brief Transfiere la matriz de modelado a la GPU
 * @param modelViewMat matriz de modelado
 */
void Abs_Entity::upload(dmat4 const& modelViewMat) const 
{ 
	glMatrixMode(GL_MODELVIEW); // Especifica qué pila de matriz es el objetivo para las operaciones de matriz posteriores
	glLoadMatrixd(value_ptr(modelViewMat));  // Reemplazan la matriz actual con otra matriz
}