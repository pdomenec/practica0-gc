/*
 * File:   EjesRGB.h
 * Author: Javier L�pez Quesada y Pedro Pablo Dom�nech Arellano
 */
#ifndef _H_EjesRGB_H_
#define _H_EjesRGB_H_

#include "Entity.h"
class EjesRGB : public Abs_Entity
{
public:
    explicit EjesRGB(GLdouble l);
    virtual void render(glm::dmat4 const& modelViewMat) const;
};

#endif // _H_EjesRGB_H_