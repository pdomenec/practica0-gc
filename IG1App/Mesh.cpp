/*
 * File:   Mesh.cpp
 * Author: Javier López Quesada y Pedro Pablo Doménech Arellano
 */
 #include "Mesh.h"
#include "CheckML.h"
#include <fstream>

using namespace std;
using namespace glm;

/**
 * @brief Dibuja la malla dada una primitiva y un tamaño
 */
void Mesh::draw() const 
{
  glDrawArrays(mPrimitive, 0, size());   // Se pasa la primitiva gráfica, la separación entre datos (útil para vectores entrelazados) 
										 // y el número de elementos que van a ser renderizados
}

/**
 * @brief Método abstracto para el renderizado de mallas
 */
void Mesh::render() const 
{
  if (vVertices.size() > 0) {  // Se trafieren los datos
    // Se transfieren las coordenada de los vértices
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_DOUBLE, 0, vVertices.data());  // Cantidad de coordenadas por vértice, tipo de cada coordenada, salto, puntero
    if (vTexCoords.size() > 0) {
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glTexCoordPointer(2, GL_DOUBLE, 0, vTexCoords.data());
    }
    if (vColors.size() > 0) { // transfer colors
      glEnableClientState(GL_COLOR_ARRAY);
      glColorPointer(4, GL_DOUBLE, 0, vColors.data());  // Número de componentes(rgba = 4), tipo de cada componente, salto, puntero
    }
    if (vNormals.size() > 0) {
        glEnableClientState(GL_NORMAL_ARRAY);
        glNormalPointer(GL_DOUBLE, 0, vNormals.data());
    }

	draw();
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
  }
}

/**
 * @brief Genera los vértices del polígono regular inscrito en la circunferencia dado su radio
 * @param numL número de vértices
 * @param rd radio de la circunferencia
 * @return malla
 */
Mesh * Mesh::generaPoligono(GLuint numL, GLdouble rd)
{
    Mesh* mesh = new Mesh();

    mesh->mPrimitive = GL_LINE_LOOP;

    mesh->mNumVertices = numL;
    mesh->vVertices.reserve(mesh->mNumVertices);

    double angle = glm::radians(360.0 / numL);
    double newAngle = glm::radians(90.0);

    for (int i = 0; i < numL; i++) {
       
        mesh->vVertices.emplace_back(rd*glm::cos(newAngle), rd*glm::sin(newAngle), 0.0);

        newAngle += angle;
  
    }

    return mesh;
}

/**
 * @brief Genera el triángulo de Sierpinski tomando un triángulo cualquiera y sus puntos medios de cada lado
 * @param rd radio de la circunferencia
 * @param numP número de vértices del triángulo equilátero de Sierpinski
 * @return malla
 */
Mesh* Mesh::generarSierpinski(GLdouble rd, GLuint numP) {
    
    Mesh* triangulo = generaPoligono(3, rd);
    Mesh* mesh = new Mesh();

    mesh->mPrimitive = GL_POINTS;

    mesh->mNumVertices = numP + 3;
    mesh->vVertices.reserve(mesh->mNumVertices);

    glm::dvec3 verticeAleatorio = triangulo->vertices()[rand() % 3];
    glm::dvec3 verticeA = triangulo->vertices()[0];
    glm::dvec3 verticeB = triangulo->vertices()[1];
    glm::dvec3 puntoMedio = (verticeA + verticeB)*0.5;

    mesh->vVertices.emplace_back(triangulo->vertices()[0]);
    mesh->vVertices.emplace_back(triangulo->vertices()[1]);
    mesh->vVertices.emplace_back(triangulo->vertices()[2]);

    for (int i = 0; i < numP; i++) {
        glm::dvec3 verticeA = triangulo->vertices()[rand() % 3];
        glm::dvec3 verticeB = mesh->vertices()[mesh->vertices().size() - 1];
        glm::dvec3 puntoMedio = (verticeA + verticeB) * 0.5;
        mesh->vVertices.emplace_back(puntoMedio);
    }

    delete triangulo; triangulo = nullptr;
    return mesh;
}

/**
 * @brief Genera un triángulo con una esquina de cada color primario
 * @param rd radio de la circunferencia
 * @return malla
 */
Mesh* Mesh::generaTrianguloRGB(GLdouble rd) {
    
    Mesh* mesh = generaPoligono(3, rd);
    
    mesh->mPrimitive = GL_TRIANGLES;

    mesh->vColors.reserve(mesh->mNumVertices);
    // Rojo
    mesh->vColors.emplace_back(1.0, 0.0, 0.0, 1.0);
    // Verde
    mesh->vColors.emplace_back(0.0, 1.0, 0.0, 1.0);
    // Azul
    mesh->vColors.emplace_back(0.0, 0.0, 1.0, 1.0);
    
    return mesh;
}

/**
 * @brief Genera un rectángulo
 * @param w ancho del rectángulo
 * @param h alto del rectángulo
 * @return malla
 */
Mesh* Mesh::generaRectangulo(GLdouble w, GLdouble h) {
    
    Mesh* mesh = new Mesh();

    mesh->mPrimitive = GL_TRIANGLE_STRIP;

    mesh->mNumVertices = 4;
    mesh->vVertices.reserve(mesh->mNumVertices);

    // V1
    mesh->vVertices.emplace_back(-w * 0.5, h * 0.5, -1.0);
    // V2
    mesh->vVertices.emplace_back(-w * 0.5, -h * 0.5, -1.0);
    // V3
    mesh->vVertices.emplace_back(w * 0.5, h * 0.5, -1.0);
    // V4
    mesh->vVertices.emplace_back(w * 0.5, -h * 0.5, -1.0);

    return mesh;
}

/**
 * @brief Añade coordenadas de textura al rectángulo
 * @param w ancho del rectángulo
 * @param h alto del rectángulo
 * @param rw número de repeticiones a lo ancho
 * @param rh número de repeticiones a lo alto
 * @return malla
 */
Mesh* Mesh::generaRectanguloTex(GLdouble w, GLdouble h, GLuint rw, GLuint rh) {

    Mesh* mesh = Mesh::generaRectangulo(w, h);

    mesh->vTexCoords.reserve(mesh->mNumVertices);
    
    // V1
    mesh->vTexCoords.emplace_back(0.0, rh);
    // V2
    mesh->vTexCoords.emplace_back(0.0, 0.0);
    // V3
    mesh->vTexCoords.emplace_back(rw, rh);
    // V4
    mesh->vTexCoords.emplace_back(rw, 0.0);

    return mesh;
}

/**
 * @brief Genera un rectángulo con una esquina de cada color primario
 * @param w ancho del rectángulo
 * @param h alto del rectángulo
 * @return malla
 */
Mesh* Mesh::generaRectanguloRGB(GLdouble w, GLdouble h) {

    Mesh* mesh = generaRectangulo(w, h);

    mesh->vColors.reserve(mesh->mNumVertices);

    mesh->vColors.emplace_back(176.0/255.0, 224.0/255.0, 230.0/255.0, 1.0);
    mesh->vColors.emplace_back(224.0 / 255.0, 1.0, 1.0, 1.0);
    mesh->vColors.emplace_back(135.0 / 255.0, 100.0 / 255.0, 250.0 / 255.0, 1.0);
    mesh->vColors.emplace_back(0.0, 191.0 / 100.0, 255.0 / 255.0, 1.0);

    return mesh;
}

/**
 * @brief Genera una estrella 3d
 * @param re radio exterior
 * @param np número de puntas
 * @param h altura
 * @return malla
 */
Mesh* Mesh::generaEstrella3D(GLdouble re, GLdouble np, GLdouble h)
{
    Mesh* mesh = new Mesh();

    mesh->mPrimitive = GL_TRIANGLE_FAN;

    mesh->mNumVertices = 2 * np + 2;
    mesh->vVertices.reserve(mesh->mNumVertices);
    mesh->vVertices.emplace_back(0.0, 0.0, 0.0);

    double angle = glm::radians(360.0 / np);
    double newAngleExt = glm::radians(90.0);
    double newAngleInt = glm::radians(90.0) + angle/2.0;
    GLdouble ri = re / 2.0;

    for (int i = 0; i < np; i++) {

        mesh->vVertices.emplace_back(re * glm::cos(newAngleExt), re * glm::sin(newAngleExt), h);
        mesh->vVertices.emplace_back(ri * glm::cos(newAngleInt), ri * glm::sin(newAngleInt), h);

        newAngleExt += angle;
        newAngleInt += angle;

    }
    mesh->vVertices.emplace_back(re * glm::cos(newAngleExt), re * glm::sin(newAngleExt), h);

    return mesh;
}

/**
 * @brief Genera los vértices del contorno de un cubo dado un lado
 * @param ld lado
 * @return malla
 */
Mesh* Mesh::generaContCubo(GLdouble ld) {
    Mesh* mesh = new Mesh();

    mesh->mPrimitive = GL_TRIANGLE_STRIP;

    GLdouble mld = ld / 2.0;

    mesh->mNumVertices = 10;
    mesh->vVertices.reserve(mesh->mNumVertices);
    mesh->vVertices.emplace_back(mld, mld, -mld);//V0
    mesh->vVertices.emplace_back(mld, -mld, -mld);//V1
    mesh->vVertices.emplace_back(mld, mld, mld);//V2
    mesh->vVertices.emplace_back(mld, -mld, mld);//V3
    mesh->vVertices.emplace_back(-mld, mld, mld);//V4
    mesh->vVertices.emplace_back(-mld, -mld, mld);//V5
    mesh->vVertices.emplace_back(-mld, mld, -mld);//V6
    mesh->vVertices.emplace_back(-mld, -mld, -mld);//V7
    mesh->vVertices.emplace_back(mld, mld, -mld);//V0
    mesh->vVertices.emplace_back(mld, -mld, -mld);//V1

    return mesh;
}

/**
 * @brief Añade coordenadas de textura a la estrella 3d
 * @param re radio exterior
 * @param np número de puntas
 * @param h altura
 * @return malla
 */
Mesh* Mesh::generaEstrellaTexCor(GLdouble re, GLuint np, GLdouble h) {
    Mesh* mesh = Mesh::generaEstrella3D(re, np, h);

    mesh->vTexCoords.reserve(mesh->mNumVertices);
    mesh->vTexCoords.emplace_back(0.5, 0.5);

    double angle = glm::radians(360.0 / np);
    double newAngleExt = glm::radians(90.0);
    double newAngleInt = glm::radians(90.0) + angle / 2.0;
    GLdouble ri = re / 2.0;

    for (int i = 0; i < np; i++) {

        mesh->vTexCoords.emplace_back(0.5 + 0.5 * glm::cos(newAngleExt), 0.5 + 0.5 * glm::sin(newAngleExt));
        mesh->vTexCoords.emplace_back(0.5 + (0.5 / 2.0) * glm::cos(newAngleInt), 0.5 + (0.5 / 2.0) * glm::sin(newAngleInt));

        newAngleExt += angle;
        newAngleInt += angle;

    }
    mesh->vTexCoords.emplace_back(0.5 + 0.5 * glm::cos(newAngleExt), 0.5 + 0.5 * glm::sin(newAngleExt));

    return mesh;
}

/**
 * @brief Añade coordenadas de textura al cubo
 * @param nl lado
 * @return malla
 */
Mesh* Mesh::generaCajaTexCor(GLdouble nl){
    Mesh* mesh = Mesh::generaContCubo(nl);
    //Mesh* meshSuelo = Mesh::generaRectangulo(nl, nl);
    mesh->vTexCoords.reserve(mesh->mNumVertices);

    mesh->vTexCoords.emplace_back(dvec2(0, 1)); // v0
    mesh->vTexCoords.emplace_back(dvec2(0, 0)); // v1
    mesh->vTexCoords.emplace_back(dvec2(1, 1)); // v2
    mesh->vTexCoords.emplace_back(dvec2(1, 0)); // v3
    mesh->vTexCoords.emplace_back(dvec2(0, 1)); //v4
    mesh->vTexCoords.emplace_back(dvec2(0, 0)); //v5
    mesh->vTexCoords.emplace_back(dvec2(1, 1)); //v6
    mesh->vTexCoords.emplace_back(dvec2(1, 0)); //v7
    mesh->vTexCoords.emplace_back(mesh->vTexCoords[0]); //v8 coincide con v0 para que cierre
    mesh->vTexCoords.emplace_back(mesh->vTexCoords[1]); //v9 coincide con v1 para que cierre

    return mesh;
}

/**
 * @brief Genera los numL vértices del polígono regular inscrito en la circunferencia de radio rd
 * @param l longitud de cada eje
 * @return malla
 */
Mesh * Mesh::createRGBAxes(GLdouble l)
{
  Mesh* mesh = new Mesh();

  mesh->mPrimitive = GL_LINES;

  mesh->mNumVertices = 6;
  mesh->vVertices.reserve(mesh->mNumVertices);

  // X axis vertices
  mesh->vVertices.emplace_back(0.0, 0.0, 0.0);
  mesh->vVertices.emplace_back(l, 0.0, 0.0);
  // Y axis vertices
  mesh->vVertices.emplace_back(0, 0.0, 0.0);
  mesh->vVertices.emplace_back(0.0, l, 0.0);
  // Z axis vertices
  mesh->vVertices.emplace_back(0.0, 0.0, 0.0);
  mesh->vVertices.emplace_back(0.0, 0.0, l);

  mesh->vColors.reserve(mesh->mNumVertices);
  // X axis color: red  (Alpha = 1 : fully opaque)
  mesh->vColors.emplace_back(1.0, 0.0, 0.0, 1.0);
  mesh->vColors.emplace_back(1.0, 0.0, 0.0, 1.0);
  // Y axis color: green
  mesh->vColors.emplace_back(0.0, 1.0, 0.0, 1.0);
  mesh->vColors.emplace_back(0.0, 1.0, 0.0, 1.0);
  // Z axis color: blue
  mesh->vColors.emplace_back(0.0, 0.0, 1.0, 1.0);
  mesh->vColors.emplace_back(0.0, 0.0, 1.0, 1.0);
 
  return mesh;
}
//-------------------------------------------------------------------------

Mesh* Mesh::generaAnilloCuadrado() {
    Mesh* mesh = new Mesh();

    mesh->mPrimitive = GL_TRIANGLE_STRIP;

    mesh->mNumVertices = 10;
    mesh->vVertices.reserve(mesh->mNumVertices);
    mesh->vVertices.emplace_back(30.0, 30.0, 0.0);//V0
    mesh->vVertices.emplace_back(10.0, 10.0, 0.0);//V1
    mesh->vVertices.emplace_back(70.0, 30.0, 0.0);//V2
    mesh->vVertices.emplace_back(90.0, 10.0, 0.0);//V3
    mesh->vVertices.emplace_back(70.0, 70.0, 0.0);//V4
    mesh->vVertices.emplace_back(90.0, 90.0, 0.0);//V5
    mesh->vVertices.emplace_back(30.0, 70.0, 0.0);//V6
    mesh->vVertices.emplace_back(10.0, 90.0, 0.0);//V7
    mesh->vVertices.emplace_back(30.0, 30.0, 0.0);//V0
    mesh->vVertices.emplace_back(10.0, 10.0, 0.0);//V1

    mesh->vColors.reserve(mesh->mNumVertices);
    mesh->vColors.emplace_back(0.0, 0.0, 0.0, 1.0);//V0
    mesh->vColors.emplace_back(1.0, 0.0, 0.0, 1.0);//V1
    mesh->vColors.emplace_back(0.0, 1.0, 0.0, 1.0);//V2
    mesh->vColors.emplace_back(0.0, 0.0, 1.0, 1.0);//V3
    mesh->vColors.emplace_back(1.0, 1.0, 0.0, 1.0);//V4
    mesh->vColors.emplace_back(1.0, 0.0, 1.0, 1.0);//V5
    mesh->vColors.emplace_back(0.0, 1.0, 1.0, 1.0);//V6
    mesh->vColors.emplace_back(1.0, 0.0, 0.0, 1.0);//V7
    mesh->vColors.emplace_back(0.0, 0.0, 0.0, 1.0);//V0
    mesh->vColors.emplace_back(1.0, 0.0, 0.0, 1.0);//V1

    return mesh;
}