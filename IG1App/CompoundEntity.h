#pragma once
#include "Entity.h"
class CompoundEntity : public Abs_Entity
{
protected:
	std::vector<Abs_Entity*> gObjects;
public:
	~CompoundEntity();
	void addEntity(Abs_Entity* ae);
	void render(glm::dmat4 const& modelViewMat) const;
};

