/*
 * File:   CajaTex.cpp
 * Author: Javier L�pez Quesada y Pedro Pablo Dom�nech Arellano
 */
#include <gtc/matrix_transform.hpp>  
#include <gtc/type_ptr.hpp>

#include "CajaCristal.h"

CajaCristal::CajaCristal(GLdouble _ld, Texture* _texturaExt, Texture* _texturaInt) {
	ld = _ld;
	mMesh = Mesh::generaCajaTexCor(ld);                   // Se genera la caja con las coordenadas de textura
	meshFondo = Mesh::generaRectanguloTex(ld, ld, 1, 1);  // Se genera un rect�ngulo con las coordenadas de textura
	mTrans = glm::dmat4(1.0);

	//Asignamos la textura exterior y la textura interior
	mTexture = _texturaExt;
	texturaInt = _texturaInt;

	entityType = "CajaTex";
	renderFondo = true;
}

/**
  * @brief Transfiere la matriz de modelado a la GPU
  * @param modelViewMat matriz de modelado
  */
void CajaCristal::render(glm::dmat4 const& modelViewMat) const {
	if (mMesh != nullptr) {
		glm::dmat4 aMat = modelViewMat * mTrans * mModelMat;  // glm matrix multiplication
		upload(aMat);
		glEnable(GL_BLEND);                                   // Se habilita el blending para crear efectos de transparencia
		glEnable(GL_ALPHA_TEST);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_CULL_FACE);								  // Se habilita la elecci�n de caras (front o back)
		mTexture->bind(GL_TEXTURE_2D);
		glCullFace(GL_FRONT);
		mMesh->render();
		mTexture->unbind();

		texturaInt->bind(GL_TEXTURE_2D);
		glCullFace(GL_BACK);
		mMesh->render();
		texturaInt->unbind();
		glDisable(GL_CULL_FACE);

		upload(aMat);
		mMesh->render();

		glDisable(GL_BLEND);
		glDisable(GL_ALPHA_TEST);

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
}
