/*
 * File:   Foto.h
 * Author: Javier L�pez Quesada y Pedro Pablo Dom�nech Arellano
 */
#pragma once
#include "Entity.h"
class Foto : public Abs_Entity
{
protected:
	GLuint widthRectangulo;
	GLuint heightRectangulo;
	glm::dmat4 mTrans;
public:

	virtual ~Foto() {
		delete mMesh;
		mMesh = nullptr;
	};

	explicit Foto(GLuint, GLuint, Texture*);
	virtual void render(glm::dmat4 const& modelViewMat) const;
	virtual void update(glm::dmat4 const& modelViewMat);
	void takePicture();
};

