#include "PosLight.h"

void PosLight::upload(glm::dmat4 const& modelViewMat) const{
	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixd(value_ptr(modelViewMat));
	glLightfv(id, GL_POSITION, value_ptr(posDir));
	glLightf(id, GL_CONSTANT_ATTENUATION, kc);
	glLightf(id, GL_LINEAR_ATTENUATION, kl);
	glLightf(id, GL_QUADRATIC_ATTENUATION, kq);
	uploadL();
}

// Ojo al 1.0 que determina que la luz sea local
void PosLight::setPosDir(glm::fvec3 dir) {
	posDir = glm::fvec4(dir, 1.0);
}

void PosLight::setAtte(GLfloat _kc, GLfloat _kl, GLfloat _kq) {
	kc = _kc;
	kl = _kl;
	kq = _kq;
}