#ifndef _H_CajaCristal_H_
#define _H_CajaCristal_H_

#include "Entity.h"
class CajaCristal : public Abs_Entity
{
protected:
	GLdouble ld;         // Lado de la caja
	glm::dmat4 mTrans;   // Matriz de transformación
	Mesh* meshFondo;     // Malla encargada del fondo, se le asignará un cuadrado
	bool renderFondo;
	Texture* texturaInt; // Textura interior de la caja
	Texture* texturaExt;
public:

	virtual ~CajaCristal() {
		delete mMesh;
		mMesh = nullptr;
		delete meshFondo;
		meshFondo = nullptr;
	};

	explicit CajaCristal(GLdouble _ld, Texture* _texturaExt, Texture* _texturaInt);
	virtual void render(glm::dmat4 const& modelViewMat) const;

	void setTexturaInt(std::string bpm_name, GLubyte alpha) {
		texturaInt->load(bpm_name, alpha);
	}

	void setTexturaExt(std::string bpm_name, GLubyte alpha) {
		mTexture->load(bpm_name, alpha);
	}

	void setRenderFondo(bool r) {
		renderFondo = r;
	}
};

#endif // _H_CajaCristal_H_