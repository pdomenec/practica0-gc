/*
 * File:   Caja.h
 * Author: Javier L�pez Quesada y Pedro Pablo Dom�nech Arellano
 */
#ifndef _H_Caja_H_
#define _H_Caja_H_

#include "Entity.h"

/**
 * @class Caja
 * @brief Clase encargada de la creaci�n de una caja en modo l�neas
 */
class Caja : public Abs_Entity
{
protected:

	GLdouble ld;        // Lado de la caja
	glm::dmat4 mTrans;  // Matriz de transformaci�n
	Mesh* meshFondo;    // Malla que pinta un cuadrado m�s como fondo

public:

	/**
	* @brief Destructor de Caja
	*/
	virtual ~Caja() {
		delete mMesh;
		mMesh = nullptr; 
		delete meshFondo;
		meshFondo = nullptr;
	};

	explicit Caja(GLdouble ld);
	virtual void render(glm::dmat4 const& modelViewMat) const;
};

#endif // _H_Caja_H_