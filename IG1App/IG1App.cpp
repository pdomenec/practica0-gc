/*
 * File:   IG1App.cpp
 * Author: Javier L�pez Quesada y Pedro Pablo Dom�nech Arellano
 */
#include "IG1App.h"
#include "CheckML.h"
#include <iostream>

using namespace std;

//-------------------------------------------------------------------------
// static single instance (singleton pattern)

IG1App IG1App::s_ig1app;  // default constructor (constructor with no parameters)

//-------------------------------------------------------------------------

void IG1App::close()  
{
	if (!mStop) {  // if main loop has not stopped
		cout << "Closing glut...\n";
		glutLeaveMainLoop();  // stops main loop and destroy the OpenGL context
		mStop = true;   // main loop stopped  
	}
	free();
}
//-------------------------------------------------------------------------

void IG1App::run()   // enters the main event processing loop
{
	if (mWinId == 0) { // if not initialized
		init();       // initialize the application 
		glutMainLoop();      // enters the main event processing loop 
		mStop = true;  // main loop has stopped  
	}
}
//-------------------------------------------------------------------------

void IG1App::init()
{
	// create an OpenGL Context
	iniWinOpenGL();   

	// create the scene after creating the context 
	// allocate memory and resources
	mViewPort = new Viewport(mWinW, mWinH); //glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT)
	mCamera = new Camera(mViewPort);
	mScene = new Scene;
	
	mCamera->set2D();
	mScene->init();
}
//-------------------------------------------------------------------------

void IG1App::iniWinOpenGL() 
{  // Initialization
	cout << "Starting glut...\n";
	int argc = 0;
	glutInit(&argc, nullptr);
		
	glutInitContextVersion(3, 3);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);  // GLUT_CORE_PROFILE
	glutInitContextFlags(GLUT_DEBUG);		// GLUT_FORWARD_COMPATIBLE

	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

	glutInitWindowSize(mWinW, mWinH);   // window size
	//glutInitWindowPosition (140, 140);

	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH /*| GLUT_STENCIL*/); // RGBA colors, double buffer, depth buffer and stencil buffer   
	
	mWinId = glutCreateWindow("IG1App");  // with its associated OpenGL context, return window's identifier 
	
	// Callback registration
	glutReshapeFunc(s_resize);
	glutKeyboardFunc(s_key);
	glutSpecialFunc(s_specialKey);
	glutDisplayFunc(s_display);
	glutIdleFunc(IG1App::s_update);
	glutMouseFunc(IG1App::s_mouse);
	glutMotionFunc(IG1App::s_motion);
	glutMouseWheelFunc(IG1App::s_mouseWheel);
	
	cout << glGetString(GL_VERSION) << '\n';
	cout << glGetString(GL_VENDOR) << '\n';
}
//-------------------------------------------------------------------------

void IG1App::free() 
{  // release memory and resources
	delete mScene; mScene = nullptr;
	delete mCamera; mCamera = nullptr;
	delete mViewPort; mViewPort = nullptr;
}
//-------------------------------------------------------------------------

void IG1App::display() const   
{  // double buffering

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  // clears the back buffer

	if (m2Vistas){
		display2Vistas();
	}
	else {
		mScene->render(*mCamera);  // uploads the viewport and camera to the GPU
	}
	
	glutSwapBuffers();	// swaps the front and back buffer
}

void IG1App::display2Vistas() const{
	Camera auxCamera = *mCamera;
	Viewport auxViewPort = *mViewPort;

	mViewPort->setSize(mWinW / 2, mWinH);
	auxCamera.setSize(mViewPort->width(), mViewPort->height());

	//camara normal
	mViewPort->setPos(0, 0);
	mScene->render(auxCamera);

	//camara cenital
	mViewPort->setPos(mWinW / 2, 0);
	auxCamera.setCenital();
	mScene->render(auxCamera);

	*mViewPort = auxViewPort;
}
//-------------------------------------------------------------------------

void IG1App::resize(int newWidth, int newHeight) 
{
	mWinW = newWidth; mWinH = newHeight;

	// Resize Viewport to the new window size
	mViewPort->setSize(newWidth, newHeight);

	// Resize Scene Visible Area such that the scale is not modified
	mCamera->setSize(mViewPort->width(), mViewPort->height()); 
}
//-------------------------------------------------------------------------

void IG1App::key(unsigned char key, int x, int y) 
{
	bool need_redisplay = true;
	
	switch (key) {
	case 27:  // Escape key 
		glutLeaveMainLoop();  // stops main loop and destroy the OpenGL context
	case '+':
		mCamera->setScale(+0.01);  // zoom in  (increases the scale)
		break;
	case '-':
		mCamera->setScale(-0.01);  // zoom out (decreases the scale)
		break;
	case 'l':
		mCamera->set3D();
		break;
	case 'o':
		mCamera->set2D();
		break;
	case 'u':
		mScene->update(*mCamera);
		break;
	case 'U':
		animacionActivada = !animacionActivada;
		break;
	case '0':
		mScene->setState(0);
		break;
	case '1':
		mScene->setState(1);
		break;
	case '2':
		mScene->setState(2);
		break;
	case '3':
		mScene->setState(3);
		break;
	case '4':
		mScene->setState(4);
		break;
	case '5':
		mScene->setState(5);
		break;
	case '6':
		mScene->setState(6);
		break;
	case '7':
		mScene->setState(7);
		break;
	case '8':
		mScene->setState(8);
		break;
	case '9':
		mScene->setState(9);
		break;
	case 'F':
		mScene->takePicture();
		need_redisplay = false;
		break;
	case 'p':
		mCamera->changePrj();
		break;
	case 'k':
		m2Vistas = !m2Vistas;
		break;
	case 'q':
		mScene->setActiveDirLigth(true);
		mScene->setActiveDarkness(false);
		break;
	case 'w':
		mScene->setActiveDirLigth(false);
		break;
	case 'Q':
		mScene->setActiveDirLigth2(true);
		mScene->setActiveDarkness(false);
		break;
	case 'W':
		mScene->setActiveDirLigth2(false);
		break;
	case 'a':
		mScene->setActivePosLigth(true);
		mScene->setActiveDarkness(false);
		break;
	case 's':
		mScene->setActivePosLigth(false);
		break;
	case 'z':
		mScene->setActiveSpotLigth(true);
		mScene->setActiveDarkness(false);
		break;
	case 'x':
		mScene->setActiveSpotLigth(false);
		break;
	case 't':
		mScene->setActiveFocoAvion(true);
		mScene->setActiveDarkness(false);
		break;
	case 'g':
		mScene->setActiveFocoAvion(false);
		break;
	case 'd':
		mScene->setActiveMinerLight(true);
		mScene->setActiveDarkness(false);
		break;
	case 'f':
		mScene->setActiveMinerLight(false);
		break;
	case 'y':
		mScene->move(*mCamera);
		break;
	case 'e':
		mScene->setActiveDirLigth(false);
		mScene->setActiveDirLigth2(false);
		mScene->setActivePosLigth(false);
		mScene->setActiveSpotLigth(false);
		mScene->setActiveFocoAvion(false);
		mScene->setActiveMinerLight(false);
		mScene->setActiveDarkness(true);
		break;

	default:
		need_redisplay = false;
		break;
	} //switch

	if (need_redisplay)
		glutPostRedisplay(); // marks the window as needing to be redisplayed -> calls to display()
}
//-------------------------------------------------------------------------

void IG1App::specialKey(int key, int x, int y) 
{
	/*
	bool need_redisplay = true;
	int mdf = glutGetModifiers(); // returns the modifiers (Shift, Ctrl, Alt)
	
	switch (key) {
	case GLUT_KEY_RIGHT:
		if (mdf == GLUT_ACTIVE_CTRL)
			mCamera->pitch(-1);   // rotates -1 on the X axis
		else
			mCamera->pitch(1);    // rotates 1 on the X axis
		break;
	case GLUT_KEY_LEFT:
		if (mdf == GLUT_ACTIVE_CTRL)
		    mCamera->yaw(1);      // rotates 1 on the Y axis 
		else 
			mCamera->yaw(-1);     // rotate -1 on the Y axis 
		break;
	case GLUT_KEY_UP:
		mCamera->roll(1);    // rotates 1 on the Z axis
		break;
	case GLUT_KEY_DOWN:
		mCamera->roll(-1);   // rotates -1 on the Z axis
		break;
	default:
		need_redisplay = false;
		break;
	}//switch

	if (need_redisplay)
		glutPostRedisplay(); // marks the window as needing to be redisplayed -> calls to display()
	*/
}
//-------------------------------------------------------------------------

void IG1App::update(void) {
	if (animacionActivada) {
		GLuint time = glutGet(GLUT_ELAPSED_TIME);
		if (time - mLastUpdateTime >= 25) {
			mScene->update(*mCamera);
			glutPostRedisplay();
			mLastUpdateTime = time;
		}

	}
}

void IG1App::mouse(int button, int state, int x, int y){
	if (state == GLUT_DOWN) {
		mMouseButt = button;
		mMouseCoord = glm::dvec2(x, mWinH - y);
	}
	
}

void IG1App::motion(int x, int y){

	glm::dvec2 mp = mMouseCoord;

	mMouseCoord = glm::dvec2(x, mWinH - y);

	mp = (mp - mMouseCoord);

	if (mMouseButt == GLUT_LEFT_BUTTON) {

		mCamera->orbit(mp.x * 0.5, mp.y);

		glutPostRedisplay();

	}
	else if (mMouseButt == GLUT_RIGHT_BUTTON) {

		mCamera->moveLR(mp.x);
		mCamera->moveUD(mp.y);

		glutPostRedisplay();

	}

}

void IG1App::mouseWheel(int n, int d, int x, int y){
	int m = glutGetModifiers();
	if (m == GLUT_ACTIVE_CTRL) {
		if (d == 1) mCamera->setScale(+0.01);
		else mCamera->setScale(-0.01);
	}
	else {
		if (d == 1) mCamera->moveFB(5);
		else mCamera->moveFB(-5);
	}
	
	glutPostRedisplay();
}