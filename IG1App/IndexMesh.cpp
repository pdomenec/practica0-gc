#include "IndexMesh.h"
//#include <set>


void IndexMesh::buildNormalVectors()
{
    this->vNormals.clear();
    this->vNormals.reserve(this->mNumVertices);
    for (int i = 0; i < this->mNumVertices; i++)
    {
        this->vNormals.emplace_back(glm::dvec3(0));
    }

    for (int i = 0; i < this->nNumIndices; i+=3)
    {
        glm::dvec3 a = this->vVertices.at(this->vIndices.at(i));
        glm::dvec3 b = this->vVertices.at(this->vIndices.at(i + 1));
        glm::dvec3 c = this->vVertices.at(this->vIndices.at(i + 2));
        glm::dvec3 n = glm::normalize(glm::cross((b - a), (c - a)));
        this->vNormals[this->vIndices.at(i)] += n;
        this->vNormals[this->vIndices.at(i + 1)] += n;
        this->vNormals[this->vIndices.at(i + 2)] += n;
    }

    for (int i = 0; i < this->mNumVertices; i++)
    {
        this->vNormals[i] = glm::normalize(this->vNormals[i]);
    }
}

void IndexMesh::generateVColors(glm::dvec4 color)
{
    this->vColors.clear();
    this->vColors.reserve(this->mNumVertices);
    for (int i = 0; i < this->mNumVertices; i++)
    {
        this->vColors.emplace_back(color);
    }
}

void IndexMesh::draw() const
{
    glDrawElements(mPrimitive, nNumIndices, GL_UNSIGNED_INT, vIndices.data());
}

void IndexMesh::render() const
{
    if (vVertices.size() > 0) {  // Se trafieren los datos
      // Se transfieren las coordenada de los v�rtices
        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3, GL_DOUBLE, 0, vVertices.data());  // Cantidad de coordenadas por v�rtice, tipo de cada coordenada, salto, puntero
        if (vTexCoords.size() > 0) {
            glEnableClientState(GL_TEXTURE_COORD_ARRAY);
            glTexCoordPointer(2, GL_DOUBLE, 0, vTexCoords.data());
        }
        if (vColors.size() > 0) { // transfer colors
            glEnableClientState(GL_COLOR_ARRAY);
            glColorPointer(4, GL_DOUBLE, 0, vColors.data());  // N�mero de componentes(rgba = 4), tipo de cada componente, salto, puntero
        }
        if (vNormals.size() > 0) {
            glEnableClientState(GL_NORMAL_ARRAY);
            glNormalPointer(GL_DOUBLE, 0, vNormals.data());
        }
        if (vIndices.size() > 0) {
            glEnableClientState(GL_INDEX_ARRAY);
            glIndexPointer(GL_UNSIGNED_INT, 0, vIndices.data());
        }
        

        draw();
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);
        glDisableClientState(GL_COLOR_ARRAY);
        glDisableClientState(GL_VERTEX_ARRAY);
        glDisableClientState(GL_NORMAL_ARRAY);
        glDisableClientState(GL_INDEX_ARRAY);
    }
}

IndexMesh* IndexMesh::generaAnilloCuadrado() {
    IndexMesh* mesh = new IndexMesh();

    mesh->mPrimitive = GL_TRIANGLE_STRIP;

    mesh->mNumVertices = 8;
    mesh->vVertices.reserve(mesh->mNumVertices);
    mesh->vVertices.emplace_back(30.0, 30.0, 0.0);//V0
    mesh->vVertices.emplace_back(10.0, 10.0, 0.0);//V1
    mesh->vVertices.emplace_back(70.0, 30.0, 0.0);//V2
    mesh->vVertices.emplace_back(90.0, 10.0, 0.0);//V3
    mesh->vVertices.emplace_back(70.0, 70.0, 0.0);//V4
    mesh->vVertices.emplace_back(90.0, 90.0, 0.0);//V5
    mesh->vVertices.emplace_back(30.0, 70.0, 0.0);//V6
    mesh->vVertices.emplace_back(10.0, 90.0, 0.0);//V7

    mesh->vColors.reserve(mesh->mNumVertices);
    mesh->vColors.emplace_back(0.0, 0.0, 0.0, 1.0);//V0
    mesh->vColors.emplace_back(1.0, 0.0, 0.0, 1.0);//V1
    mesh->vColors.emplace_back(0.0, 1.0, 0.0, 1.0);//V2
    mesh->vColors.emplace_back(0.0, 0.0, 1.0, 1.0);//V3
    mesh->vColors.emplace_back(1.0, 1.0, 0.0, 1.0);//V4
    mesh->vColors.emplace_back(1.0, 0.0, 1.0, 1.0);//V5
    mesh->vColors.emplace_back(0.0, 1.0, 1.0, 1.0);//V6
    mesh->vColors.emplace_back(1.0, 0.0, 0.0, 1.0);//V7
    // glm::dvec3 n=normalize(cross((v2-v1),(v0-v1)))
    glm::dvec3 n = glm::normalize(glm::cross(glm::dvec3(70.0, 30.0, 0.0) - glm::dvec3(10.0, 10.0, 0.0), (glm::dvec3(30.0, 30.0, 0.0) - glm::dvec3(10.0, 10.0, 0.0))));

    mesh->vNormals.reserve(mesh->mNumVertices);
    for (int i = 0; i < mesh->mNumVertices; i++)
    {
        mesh->vNormals.emplace_back(n);
    } 

    mesh->nNumIndices = 10;
    for (int i = 0; i < mesh->nNumIndices; i++)
    {
        mesh->vIndices.push_back(i % mesh->mNumVertices);
    }

    return mesh;
}
#include <iostream>
IndexMesh* IndexMesh::generaIndexCuboConTapas(GLdouble l, glm::dvec4 color)
{

    IndexMesh* mesh = new IndexMesh();

    mesh->mPrimitive = GL_TRIANGLES;

    glm::dvec3 v[8] = {
        { l, l, l },
        { l, -l, l },
        { l, l, -l },
        { l, -l, -l },
        { -l, l, -l },
        { -l, -l, -l },
        { -l, l, l },
        { -l, -l, l }
    };

    mesh->mNumVertices = 8;
    mesh->vVertices.reserve(mesh->mNumVertices);
    mesh->vNormals.reserve(mesh->mNumVertices);
    //mesh->vColors.reserve(mesh->mNumVertices);
    //std::vector<glm::dvec3> borrar;
    //borrar.reserve(mesh->mNumVertices);
    for (int i = 0; i < mesh->mNumVertices; i++)
    {
        mesh->vVertices.emplace_back(v[i]);
        mesh->vNormals.emplace_back(glm::normalize(v[i]));
        //mesh->vColors.emplace_back(color);
        //borrar.emplace_back(glm::normalize(v[i]));
    }

    mesh->nNumIndices = 36;
    GLuint indices[36] = {
        0,1,2, 2,1,3,
        2,3,4, 4,3,5,
        6,4,7, 7,4,5,
        6,7,0, 0,7,1,
        4,6,2, 2,6,0,
        1,7,3, 3,7,5
    };
    mesh->vIndices.reserve(mesh->nNumIndices);
    for (int i = 0; i < mesh->nNumIndices; i++)
    {
        mesh->vIndices.emplace_back(indices[i]);
    }
    /*
    mesh->buildNormalVectors();
    
    for (size_t i = 0; i < mesh->mNumVertices; i++)
    {
        std::cout << mesh->vNormals[i].x << " " << mesh->vNormals[i].y << " " << mesh->vNormals[i].z << " == " << borrar[i].x << " " << borrar[i].y << " " << borrar[i].z << std::endl;
    }
    */

    return mesh;
}
