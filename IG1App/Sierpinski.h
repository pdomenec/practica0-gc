/*
 * File:   Sierpinski.h
 * Author: Javier L�pez Quesada y Pedro Pablo Dom�nech Arellano
 */
 //#pragma once
#ifndef _H_Sierpinski_H_
#define _H_Sierpinski_H_

#include "Entity.h"

class Sierpinski : public Abs_Entity
{
public:
    explicit Sierpinski(GLdouble, GLuint);
    virtual void render(glm::dmat4 const& modelViewMat) const;
};

#endif // _H_Sierpinski_H_