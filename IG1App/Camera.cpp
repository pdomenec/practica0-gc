/*
 * File:   Camera.cpp
 * Author: Javier López Quesada y Pedro Pablo Doménech Arellano
 */
 #include "Camera.h"

#include <gtc/matrix_transform.hpp>  
#include <gtc/type_ptr.hpp>
#include <gtc/matrix_access.hpp>

using namespace glm;

/**
 * @brief Constructor parametrizado de Camera
 * @param vp viewport
 */
Camera::Camera(Viewport* vp): mViewPort(vp), mViewMat(1.0), mProjMat(1.0),  
							  xRight(vp->width() / 2.0), xLeft(-xRight),
							  yTop(vp->height() / 2.0), yBot(-yTop)
{
    setAxes();
    setPM(); // Inicializa la matriz de proyección
}

/**
 * @brief Transfiere la matriz de visión a la GPU
 */
void Camera::uploadVM() const 
{
	glMatrixMode(GL_MODELVIEW); // Especifica qué pila de matriz es el objetivo para las operaciones de matriz posteriores (GL_MODELVIEW, GL_PROJECTION, GL_TEXTURE o GL_COLOR)
	glLoadMatrixd(value_ptr(mViewMat)); // Reemplazan la matriz actual con una matriz arbitraria
}

/**
 * @brief Actualiza la matriz de visión con los nuevos valores de eye, look y up
 */
void Camera::setVM() 
{
	mViewMat = lookAt(mEye, mLook, mUp);  // glm::lookAt define la matriz de visión 
    setAxes();
}

/**
 * @brief Modifica la matriz de visión para una vista en 2D
 */
void Camera::set2D() 
{
	mEye = dvec3(0, 0, mRadio); // Al situar la cámara en el eje Z y apuntando al origen de coordenadas se vería el eje X y el Y
	mLook = dvec3(0, 0, 0);
	mUp = dvec3(0, 1, 0);
    mAng = 270;
	setVM();
}

/**
 * @brief Modifica la matriz de visión para una vista en 3D
 */
void Camera::set3D() 
{
    mAng = 315;
	mEye = dvec3(cos(radians(mAng)) * mRadio, mRadio, -sin(radians(mAng)) * mRadio); // Se ven los 3 ejes
	mLook = dvec3(0, 0, 0);   
	mUp = dvec3(0, 1, 0);
    
	setVM();
}

///**
// * @brief Rota a grados en el eje X
// * @param a ángulo de rotación
// */
//void Camera::pitch(GLdouble a) 
//{  
//	mViewMat = rotate(mViewMat, glm::radians(a), glm::dvec3(1.0, 0, 0)); // glm::rotate devuelve mViewMat * rotationMatrix
//}
//
///**
// * @brief Rota a grados en el eje Y
// * @param a ángulo de rotación
// */
//void Camera::yaw(GLdouble a) 
//{
//	mViewMat = rotate(mViewMat, glm::radians(a), glm::dvec3(0, 1.0, 0)); // glm::rotate devuelve mViewMat * rotationMatrix
//}
//
///**
// * @brief Rota a grados en el eje Z
// * @param a ángulo de rotación
// */
//void Camera::roll(GLdouble a) 
//{
//	mViewMat = rotate(mViewMat, glm::radians(a), glm::dvec3(0, 0, 1.0)); // glm::rotate devuelve mViewMat * rotationMatrix
//}

/**
 * @brief Modifica el tamaño del área visible de la escena 
 * @param xw ancho
 * @param yh alto
 */
void Camera::setSize(GLdouble xw, GLdouble yh) 
{
	xRight = xw / 2.0;
	xLeft = -xRight;
	yTop = yh / 2.0;
	yBot = -yTop;
	setPM();
}

/**
 * @brief Modifica el factor de escalado
 * @param s factor de escalado
 */
void Camera::setScale(GLdouble s) 
{
	mScaleFact -= s;
	if (mScaleFact < 0) mScaleFact = 0.01;
	setPM();
}

/**
 * @brief  Actualiza la matriz de proyección con los nuevos valores del tamaño del área y del volumen de vista
 */
void Camera::setPM() 
{
    if (bOrto) { // Si hay proyección ortogonal
        mProjMat = ortho(xLeft * mScaleFact, xRight * mScaleFact, yBot * mScaleFact, yTop * mScaleFact, mNearVal, mFarVal); // glm::ortho define la matriz de proyección ortogonal
    }
    else { // Si hay proyección perspectiva
        mProjMat = frustum(xLeft * mScaleFact, xRight * mScaleFact, yBot * mScaleFact, yTop * mScaleFact, mNearVal, mFarVal); // glm::ortho define la matriz de proyección ortogonal
    }
}

/**
 * @brief Transfiere la matriz de proyección a la GPU
 */
void Camera::uploadPM() const 
{
	glMatrixMode(GL_PROJECTION); // Especifica qué pila de matriz es el objetivo para las operaciones de matriz posteriores (GL_MODELVIEW, GL_PROJECTION, GL_TEXTURE o GL_COLOR)
	glLoadMatrixd(value_ptr(mProjMat)); // Reemplazan la matriz actual con una matriz arbitraria
	glMatrixMode(GL_MODELVIEW); // Una vez se transfiere la matriz de proyección se vuelve a modelView
}

/**
 * @brief La cámara se mueve a la derecha o a la izquierda
 * @param cs distancia desplazada
 */
void Camera::moveLR(GLdouble cs) {
    mEye += mRight * cs;
    mLook += mRight * cs;
    setVM();
}

/**
 * @brief La cámara se mueve hacia detrás y hacia delante
 * @param cs distancia desplazada
 */
void Camera::moveFB(GLdouble cs) {
    mEye += mFront * cs;
    mLook += mFront * cs;
    setVM();
}

/**
 * @brief La cámara se mueve arriba o abajo
 * @param cs distancia desplazada
 */
void Camera::moveUD(GLdouble cs) {
    mEye += mUpward * cs;
    mLook += mUpward * cs;
    setVM();
}

/**
 * @brief Da valor a los tres ejes
 */
void Camera::setAxes() {
    mRight = glm::row(mViewMat, 0);
    mUpward = glm::row(mViewMat, 1);
    mFront = -glm::row(mViewMat, 2);
}

/**
 * @brief La cámara se desplaza en órbita
 * @param incAng ángulo de inclinación
 * @param incY inclinación en y
 */
void Camera::orbit(GLdouble incAng, GLdouble incY) {

    mAng += incAng;
    mEye.x = mLook.x + cos(radians(mAng)) * mRadio;
    mEye.z = mLook.z - sin(radians(mAng)) * mRadio;
    mEye.y += incY;
    setVM();

}

/**
 * @brief Cambia de proyección ortogonal a perspectiva
 */
void Camera::changePrj() {

    bOrto = !bOrto;
    if (bOrto) { // Si hay proyección ortogonal
        mNearVal = 1;
    }
    else { // Si hay proyección perspectiva
        mNearVal = yTop * mScaleFact * 2;
    }
    setPM();
}

/**
 * @brief Muestra una vista cenital de la escena
 */
void Camera::setCenital()
{
    mEye = dvec3(0, mRadio, 0); // Situamos la cámara a la altura del radio
    mLook = dvec3(0, 0, 0);     // Mirando hacia el origen de coordenadas
    mUp = dvec3(0, 0, -1);      // Cambia el vector arriba que ahora apunta a -Z (o Z)
    mAng = -90.0;               // Con un ángulo de -90 grados (o 90)
    mViewMat = lookAt(mEye, mLook, mUp);
}
