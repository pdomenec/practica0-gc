/*
 * File:   Camera.h
 * Author: Javier López Quesada y Pedro Pablo Doménech Arellano
 */
//#pragma once
#ifndef _H_Camera_H_
#define _H_Camera_H_

#include <GL/freeglut.h>
#include <glm.hpp>

#include "Viewport.h"

/**
 * @class Camera
 * @brief Clase encargada de la cámara
 */
class Camera {
public:

	explicit Camera(Viewport* vp); //Constructor parametrizado de Camera
	
	/**
	* @brief Destructor de Camera
	*/
	~Camera() {
		
	};
	
	/**
	* @brief Devuelve el viewPort (ventana gráfica)
	* @return mViewPort viewport
	*/
	Viewport const& viewPort() const { 
		return *mViewPort;
	};
	
	/**
	* @brief Devuelve la matriz de visión
	* @return matriz de visión
	*/
	glm::dmat4 const& viewMat() const { 
		return mViewMat;
	};
	
	void set2D(); //Modifica la matriz de visión para una vista en 2D
	void set3D(); //Modifica la matriz de visión para una vista en 3D
	
	//void pitch(GLdouble a); // Rota a grados en el eje X
	//void yaw(GLdouble a);   // Rota a grados en el eje Y
	//void roll(GLdouble a);  // Rota a grados en el eje Z
	
	/**
	* @brief Devuelve la matriz de proyección
	* @return matriz de proyección
	*/
	glm::dmat4 const& projMat() const { 
		return mProjMat;
	};
	
	void setSize(GLdouble xw, GLdouble yh); // Modifica el tamaño del área visible de la escena 
	
	void setScale(GLdouble s); // Modifica el factor de escalado

	/**
	* @brief Transfiere su ventana gráfica, la matriz de vista y la matriz de proyección a la GPU
	*/
	void upload() const {
		mViewPort->upload();
		uploadVM(); 
		uploadPM();
	}; 

	void moveLR(GLdouble cs);
	void moveFB(GLdouble cs);
	void moveUD(GLdouble cs);

	void orbit(GLdouble incAng, GLdouble incY);

	void setBOrto(bool b) { bOrto = b; };
	bool getBOrto() { return bOrto; };
	void changePrj();
	void setCenital(); //Muestra una vista cenital de la escena

protected:
	
	glm::dvec3 mEye = { 0.0, 0.0, 500.0 };  // Posición de la cámara
	glm::dvec3 mLook = { 0.0, 0.0, 0.0 };   // Punto al que mira la cámara (target)
	glm::dvec3 mUp = { 0.0, 1.0, 0.0 };     // Vector arriba (normalmente (0, 1, 0))

	glm::dmat4 mViewMat;    // Matriz de visión (inversa de la matriz de modelado) 
	void uploadVM() const;  // Transfiere la matriz de visión a la GPU

	glm::dmat4 mProjMat;     // Matriz de proyección
	void uploadPM() const;   // Transfiere la matriz de proyección a la GPU

	GLdouble xRight, xLeft, yTop, yBot;      // Tamaño del área visible de la escena
	GLdouble mNearVal = 1, mFarVal = 10000;  // Marca el límite de recorte de la escena más cercano y más lejano (volumen de vista)
	GLdouble mScaleFact = 1;   // Factor de escalado
	bool bOrto = true;   // Proyección ortogonal o en perspectiva
	GLdouble mAng = 0; // Angulo de inclinacion de la camara
	GLdouble mRadio = 1000.0; // Radio de la orbita de la camara

	Viewport* mViewPort;   // Ventana gráfica

	void setVM(); // Actualiza la matriz de visión
	void setPM(); // Actualiza la matriz de proyección

	glm::dvec3 mRight, mUpward, mFront; // Ejes de la cámara
	void setAxes(); // Da valor a los tres ejes
};

#endif //_H_Camera_H_