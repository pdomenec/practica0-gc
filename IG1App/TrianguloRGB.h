/*
 * File:   TrianguloRGB.h
 * Author: Javier L�pez Quesada y Pedro Pablo Dom�nech Arellano
 */
 //#pragma once
#ifndef _H_TrianguloRGB_H_
#define _H_TrianguloRGB_H_

#include "Entity.h"

class TrianguloRGB : public Abs_Entity
{
protected:
    glm::dvec3 translateVec;
    GLdouble radio;
    GLdouble angleRot;
    GLdouble angleTrans;
    glm::dmat4 mTrans;
public:
    explicit TrianguloRGB(GLdouble);
    virtual void render(glm::dmat4 const& modelViewMat) const;
    virtual void update(glm::dmat4 const& modelViewMat);

    void setTranslateVec(glm::dvec3 m) {
        this->translateVec = m;
    }
    glm::dvec3 getTranslateVec() {
        return this->translateVec;
    }
};

#endif // _H_TrianguloRGB_H_