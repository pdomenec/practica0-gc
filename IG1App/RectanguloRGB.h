/*
 * File:   RectanguloRGB.h
 * Author: Javier L�pez Quesada y Pedro Pablo Dom�nech Arellano
 */
#ifndef _H_RectanguloRGB_H_
#define _H_RectanguloRGB_H_

#include "Entity.h"

class RectanguloRGB : public Abs_Entity
{
public:
    explicit RectanguloRGB(GLdouble, GLdouble);
    virtual void render(glm::dmat4 const& modelViewMat) const;
};

#endif // _H_TrianguloRGB_H_