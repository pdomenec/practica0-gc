#include "AnilloCuadrado.h"

AnilloCuadrado::AnilloCuadrado() {
	mMesh = IndexMesh::generaAnilloCuadrado();
	entityType = "AnilloCuadrado";
}

void AnilloCuadrado::render(glm::dmat4 const& modelViewMat) const
{
	if (mMesh != nullptr) {
		glm::dmat4 aMat = modelViewMat * mModelMat;  // glm matrix multiplication
		//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glEnable(GL_COLOR_MATERIAL);
		upload(aMat);
		mMesh->render();
		glDisable(GL_COLOR_MATERIAL);
		//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
}


