/*
 * File:   Viewport.h
 * Author: Javier López Quesada y Pedro Pablo Doménech Arellano
 */
//#pragma once
#ifndef _H_Viewport_H_
#define _H_Viewport_H_

#include <GL/freeglut.h>
#include <glm.hpp>

/**
 * @class Viewport
 * @brief Clase encargada de la gestión de la ventana gráfica
 */
class Viewport 
{  
public:

	/**
	* @brief Constructor parametrizado de Viewport
	* @param xw ancho
	* @param yh alto
	*/
	Viewport(GLsizei xw, GLsizei yh): xWidth(xw), yHeight(yh) {
		
	};
	
	/**
	* @brief Destructor de Viewport
	*/
	~Viewport() {
		
	};
	
	/**
	* @brief Devuelve la posición izquierda de la ventana
	* @return posición izquierda x
	*/
	GLsizei left() const { 
		return xLeft;
	};
	
	/**
	* @brief Devuelve la posición inferior de la ventana
	* @return posición inferior y
	*/
	GLsizei bot() const { 
		return yBot;
	};    
	
	/**
	* @brief Devuelve el ancho de la ventana
	* @return ancho
	*/
	GLsizei width() const { 
		return xWidth;
	};     
	
	/**
	* @brief Devuelve el alto de la ventana
	* @return alto
	*/
	GLsizei height() const { 
		return yHeight;
	};      

	void setPos(GLsizei xl, GLsizei yb);    // Modifica la posición de la ventana (izquierda e inferior)
	void setSize(GLsizei xw, GLsizei yh);   // Modifica el tamaño de la ventana (ancho y alto)
	
	void upload() const;   // Transfiere el viewport a la GPU

protected:

	GLint xLeft = 0, yBot = 0;   // Posición izquierda e inferior de la ventana gráfica
	GLsizei xWidth, yHeight;     // Ancho y alto de la ventana gráfica
};

#endif //_H_Viewport_H_
