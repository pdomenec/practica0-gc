/*
 * File:   Viewport.cpp
 * Author: Javier López Quesada y Pedro Pablo Doménech Arellano
 */
#include "Viewport.h"

/**
 * @brief Modifica el tamaño de la ventana gráfica
 * @param xw ancho
 * @param yh alto
 */
void Viewport::setSize(GLsizei xw, GLsizei yh) 
{
	xWidth = xw; 
	yHeight = yh; 
}

/**
 * @brief Modifica la posición de la ventana gráfica
 * @param xl posición izquierda
 * @param yb posición inferior
 */
void Viewport::setPos(GLsizei xl, GLsizei yb) 
{
	xLeft = xl; 
	yBot = yb;
}

/**
 * @brief Transfiere el viewport a la GPU
 */
void Viewport::upload() const 
{
	glViewport(xLeft, yBot, xWidth, yHeight); // Transfiere el viewport a la GPU
}


