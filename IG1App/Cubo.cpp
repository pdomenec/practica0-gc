#include <gtc/matrix_transform.hpp>  
#include <gtc/type_ptr.hpp>

#include "Cubo.h"

Cubo::Cubo(GLdouble l, glm::dvec4 color) {
	mMesh = IndexMesh::generaIndexCuboConTapas(l, color);
	this->color = { color.r ,color.g ,color.b };
	entityType = "Cubo";
}

void Cubo::render(glm::dmat4 const& modelViewMat) const
{
	if (mMesh != nullptr) {
		glm::dmat4 aMat = modelViewMat * mModelMat;  // glm matrix multiplication
		upload(aMat);
		if (glIsEnabled(GL_LIGHT0)) {
			setCopper();
		}
		else {
			glEnable(GL_COLOR_MATERIAL);
			glColor3f(color.r, color.g, color.b);
		}
		
		mMesh->render();
		if (!glIsEnabled(GL_LIGHT0)) {
			glColor3f(1.0, 1.0, 1.0);
			glDisable(GL_COLOR_MATERIAL);
		}

	}
}

void Cubo::setCopper() const {
	glm::fvec3 ambient = { 0.19125, 0.0735, 0.0225 };
	glm::fvec3 diffuse = { 0.7038, 0.27048, 0.0828 };
	glm::fvec3 specular = { 0.256777, 0.137622, 0.086014 };
	glMaterialfv(GL_FRONT, GL_AMBIENT, value_ptr(ambient));
	glMaterialfv(GL_FRONT, GL_DIFFUSE, value_ptr(diffuse));
	glMaterialfv(GL_FRONT, GL_SPECULAR, value_ptr(specular));
	glMaterialf(GL_FRONT, GL_SHININESS, 12.8);
}
