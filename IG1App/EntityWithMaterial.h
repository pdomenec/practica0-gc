#pragma once
#include "Entity.h"
#include "Material.h"

class EntityWithMaterial : public Abs_Entity
{
protected:
	Material* material = nullptr;
public:
	EntityWithMaterial() : Abs_Entity() {};
	virtual ~EntityWithMaterial() { 
		if (material) {
			delete material;
			material = nullptr;
		}
	};

	void setMaterial(Material* matl) { material = matl; };

};

