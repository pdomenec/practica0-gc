#include "Chasis.h"
#include <gtc/matrix_transform.hpp>  
#include <gtc/type_ptr.hpp>

Chasis::Chasis()
{
	this->helices = new Helices();

	this->bola = new Sphere(100);
	this->bola->setColor({ 1, 0, 0 });

	this->helices->setModelMat(
		glm::translate(this->helices->modelMat(), glm::dvec3(0, 0, 115)) *
		glm::rotate(glm::dmat4(1.0), glm::radians(90.0), glm::dvec3(0, 1, 0))
	);

	this->gObjects.push_back(this->bola);
	this->gObjects.push_back(this->helices);
	this->entityType = "Chasis";
}

void Chasis::update(glm::dmat4 const& modelViewMat) {
	helices->update(modelViewMat);
}
