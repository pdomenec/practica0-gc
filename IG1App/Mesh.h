/*
 * File:   Mesh.h
 * Author: Javier López Quesada y Pedro Pablo Doménech Arellano
 */
 //#pragma once
#ifndef _H_Mesh_H_
#define _H_Mesh_H_

#include <GL/freeglut.h>
#include <glm.hpp>

#include <vector>

/**
 * @class Mesh
 * @brief Clase encargada de la malla
 */
class Mesh 
{
public:

	static Mesh* createRGBAxes(GLdouble l); // Crea una nueva malla de ejes 3D-RGB
	
	/**
	* @brief Constructor por defecto de Mesh
	*/
	Mesh() {
		
	};
	
	/**
	* @brief Destructor de Mesh
	*/
	virtual ~Mesh() {
		
	};

	Mesh(const Mesh & m) = delete;  // No tiene constructor de copia
	Mesh & operator=(const Mesh & m) = delete;  // // No tiene operador de asignación
			
	virtual void render() const; // Método abstracto para el renderizado de mallas
	
	/**
	* @brief Devuelve el número de vértices de la malla
	* @return número de vértices
	*/
	GLuint size() const { 
		return mNumVertices;
	};
	
	/**
	* @brief Devuelve el vector de vértices de la malla
	* @return vector de vértices
	*/
	std::vector<glm::dvec3> const& vertices() const { 
		return vVertices;
	};
	
	/**
	* @brief Devuelve el vector de colores de la malla
	* @return vector de colores
	*/
	std::vector<glm::dvec4> const& colors() const { 
		return vColors;
	};
	
	static Mesh* generaPoligono(GLuint numL, GLdouble rd); // Genera un polígono  

	static Mesh* generarSierpinski(GLdouble rd, GLuint numP); // Generar triángulo de Sierpinski

	static Mesh* generaTrianguloRGB(GLdouble rd); // Generear un triángulo relleno con degradado RGB

	static Mesh* generaRectangulo(GLdouble w, GLdouble h); // Generar un rectángulo

	static Mesh* generaRectanguloTex(GLdouble w, GLdouble h, GLuint rw, GLuint rh); // Generar un rectángulo con textura

	static Mesh* generaRectanguloRGB(GLdouble w, GLdouble h); // Generar un rectángulo relleno con degradado RGB

	static Mesh* generaEstrella3D(GLdouble re, GLdouble np, GLdouble h); // Genera una estrella de np puntas en Z=h con radio exterior re y radio interior re/2

	static Mesh* generaContCubo(GLdouble ld); // Genera el contorno de un cubo

	static Mesh* generaEstrellaTexCor(GLdouble re, GLuint np, GLdouble h); // Generar textura para la estrella

	static Mesh* generaCajaTexCor(GLdouble nl); // Generar textura para la caja

	static Mesh* generaAnilloCuadrado();
		
protected:
	
	GLuint mPrimitive = GL_TRIANGLES;   // Primitivas gráficas: GL_POINTS, GL_LINES, GL_TRIANGLES, ...
	GLuint mNumVertices = 0;  			// Número de elementos ( = vVertices.size())
	std::vector<glm::dvec3> vVertices;  // Vector de vértices
	std::vector<glm::dvec4> vColors;    // Vector de colores
	std::vector<glm::dvec2> vTexCoords;	// Vector de texturas
	std::vector<glm::dvec3> vNormals;	// Vector de normales
	virtual void draw() const; 			// Método abstracto para dibujar las mallas
};
//-------------------------------------------------------------------------

#endif //_H_Scene_H_