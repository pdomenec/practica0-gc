#pragma once
#include "Mesh.h"
#include <vector>

class IndexMesh : public Mesh
{
protected:
    int nNumIndices = 0;
    std::vector<GLuint> vIndices;
    
public:
    IndexMesh()
    {
        
    }

    void draw() const;
    void render() const;
    void buildNormalVectors();
    void generateVColors(glm::dvec4 color);


    static IndexMesh* generaAnilloCuadrado();

    static IndexMesh* generaIndexCuboConTapas(GLdouble l, glm::dvec4 color);
    
};

