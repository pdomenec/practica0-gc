/*
 * File:   Scene.h
 * Author: Javier L�pez Quesada y Pedro Pablo Dom�nech Arellano
 */
 //#pragma once
#ifndef _H_Scene_H_
#define _H_Scene_H_

#include <GL/freeglut.h>
#include <glm.hpp>

#include "Camera.h"
#include "Entity.h"
#include "EjesRGB.h"
#include "Poligono.h"
#include "Sierpinski.h"
#include "TrianguloRGB.h"
#include "RectanguloRGB.h"
#include "Estrella3D.h"
#include "Estrella3DTex.h"
#include "Caja.h"
#include "CajaTex.h"
#include "Texture.h"
#include "Suelo.h"
#include "Foto.h"
#include "CajaCristal.h"
#include "Sphere.h"
#include "Cylinder.h"
#include "Disk.h"
#include "PartialDisk.h"
#include "AnilloCuadrado.h"
#include "EntityWithIndexMesh.h"
#include "Cubo.h"
#include "Avion.h"
#include "Cono.h"
#include "Esfera.h"

#include <vector>
#include "DirLight.h"
#include "PosLight.h"
#include "SpotLight.h"

//-------------------------------------------------------------------------

class Scene	
{ 
public:
	Scene() {
		mId = 0;
		activeDirLight = false;
		activeDirLight2 = false;
		activePosLight = false;
		activeSpotLight = false;
		activeDarkness = false;
		activeFocoAvion = false;
		activeMinerLight = false;
		directionalLight = new DirLight();
		positionalLight = new PosLight();
		positionalLight->setPosDir({ 300, 300, 0 });
		positionalLight->setDiff({ 0.0, 1, 0.0, 1 });
		spotSceneLight = new SpotLight();
		spotSceneLight->setPosDir({ 0, 0, 300 });
		spotSceneLight->setDiff({ 0.0, 1, 0.0, 1 });
		spotSceneLight->setSpot({ 0, 0, -1 }, 45.0, 4.0);
		directionalLight2 = new DirLight();
		minerLight = new PosLight();
		minerLight->setPosDir({ 0, 0, 0 });
		minerLight->setDiff({ 1, 1, 1, 1 });
	};
	~Scene() { free(); resetGL(); };

	Scene(const Scene& s) = delete;  // no copy constructor
	Scene& operator=(const Scene& s) = delete;  // no copy assignment
		
	void init();
	void update(Camera const& cam); //Continua la animaci�n

    void render(Camera const& cam) const;

	void setState(int id);
	void takePicture();
	void setActiveDirLigth(bool show);
	void setActiveDirLigth2(bool show);
	void setActivePosLigth(bool show); 
	void setActiveSpotLigth(bool show);
	void setActiveFocoAvion(bool show);
	void setActiveMinerLight (bool show);
	void setActiveDarkness(bool show);
	void setLights(glm::dmat4 const& modelViewMat) const;
	void move(Camera const& cam);
	
protected:
	void free();
	void setGL();
	void resetGL();

	std::vector<Abs_Entity*> gObjects;  // Entities (graphic objects) of the scene
	std::vector<Texture*> gTextures;	// vector de texturas de la escena

private:

	int mId;
	bool activeDirLight;
	bool activeDirLight2;
	bool activePosLight;
	bool activeSpotLight;
	bool activeFocoAvion;
	bool activeMinerLight;
	bool activeDarkness;

	DirLight* directionalLight = nullptr;
	PosLight* positionalLight = nullptr;
	SpotLight* spotSceneLight = nullptr;
	DirLight* directionalLight2 = nullptr;
	PosLight* minerLight = nullptr;

	void ejesRGB();
	void poligonoRegular();
	void trianguloDeSierpinski();
	void trianguloRGB();
	void rectangulo();
	void composicionEscena2D();
	void estrella3D();
	void caja();
	void cajaTex();
	void estrella3DTex();
	void sueloTex();
	void composicionEscena3D();
	void foto();
	void cristalTranslucido();
	void composicionEscena3DCristalera();
	void planta();
	void quadricScene1();

	void sceneDirLight(Camera const& cam) const;
	void scenePosLight(Camera const& cam) const;
	void sceneSpotLight(Camera const& cam) const;
	void sceneDarkness(Camera const& cam) const;
	void anilloCuadradoScene1();
	void cuboIndexMesh();
	void cuboEntity();
	void avionScene();
	void conoMbR();
	void esferaMbR();
	void sphereVsEsfera();
	void avionSobreEsfera();
	void avionSobreEsferaMaterial();
	Avion* getAvion() const;

};
//-------------------------------------------------------------------------

#endif //_H_Scene_H_

