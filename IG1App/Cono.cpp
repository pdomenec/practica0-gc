#include "Cono.h"

Cono::Cono(GLdouble h, GLdouble r, GLint n)
{
	std::vector<glm::dvec3> perfil;
	perfil.push_back({ 0.5, 0, 0 });
	perfil.push_back({ 0, 0, r });
	perfil.push_back({ 0.5, h, 0 });
	this->mMesh = MbR::generaIndexMeshByRevolution(n, 3, perfil);
}

void Cono::generateVColors()
{
	this->mMesh->generateVColors(this->mColor);
}

void Cono::render(glm::dmat4 const& modelViewMat) const
{
	if (mMesh != nullptr) {
		glm::dmat4 aMat = modelViewMat * mModelMat;  // glm matrix multiplication
		upload(aMat);
		glEnable(GL_COLOR_MATERIAL);
		glPolygonMode(GL_FRONT, GL_FILL);
		//glColor3f(color.r, color.g, color.b);
		mMesh->render();
		//glColor3f(1.0, 1.0, 1.0);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glDisable(GL_COLOR_MATERIAL);
	}
}

