#include "MbR.h"

MbR::MbR(int numMuestras, int numPuntosPerfil, std::vector<glm::dvec3> perfil)
{
    this->numMuestras = numMuestras;
    this->numPuntosPerfil = numPuntosPerfil;
    this->perfil = perfil;
}

MbR* MbR::generaIndexMeshByRevolution(int numMuestras, int numPuntosPerfil, std::vector<glm::dvec3> perfil)
{
	MbR* mesh = new MbR(numMuestras, numPuntosPerfil, perfil);

    mesh->mPrimitive = GL_TRIANGLES;

    //INICIO GENERA VERTICES
    mesh->mNumVertices = numPuntosPerfil * numMuestras;
    mesh->vVertices.reserve(mesh->mNumVertices);
    for (int i = 0; i < numMuestras; i++) {
        // Generar la muestra i-�sima de v�rtices
        GLdouble theta = i * 360 / numMuestras;
        GLdouble c = cos(glm::radians(theta));
        GLdouble s = sin(glm::radians(theta));
        // R_y(theta) es la matriz de rotaci�n alrededor del eje Y
        for (int j = 0; j < numPuntosPerfil; j++) {
            //int indice = i * numMuestras + j;
            GLdouble x = c * perfil[j].x + s * perfil[j].z;
            GLdouble z = -s * perfil[j].x + c * perfil[j].z;
            mesh->vVertices.emplace_back(glm::dvec3(x, perfil[j].y, z));
        }
    }
    //FIN GENERA VERTICES

    //INICIO GENERA INDICES
    
    // puntos del perfil *
    // numero de muestras *
    // puntos de un triangulo *
    // nuemro de triangulos en un cuadrilatero
    mesh->nNumIndices = (numPuntosPerfil-1) * numMuestras * 3 * 2;
    mesh->vIndices.reserve(mesh->nNumIndices);
    // sentido antihorario
    for (int i = 0; i < numMuestras; i++)
    {
        for (int j = 0; j < numPuntosPerfil-1; j++)
        {
            int indice = i * numPuntosPerfil + j;
            mesh->vIndices.emplace_back(indice);
            mesh->vIndices.emplace_back((indice + numPuntosPerfil) % (numPuntosPerfil * numMuestras));
            mesh->vIndices.emplace_back((indice + numPuntosPerfil + 1) % (numPuntosPerfil * numMuestras));


            mesh->vIndices.emplace_back((indice + numPuntosPerfil + 1) % (numPuntosPerfil * numMuestras));
            mesh->vIndices.emplace_back(indice + 1);
            mesh->vIndices.emplace_back(indice);
        }
       
    }
    //FIN GENERA INDICES

    //CONSTRUIR NORMALES
    mesh->buildNormalVectors();

	return mesh;
}
