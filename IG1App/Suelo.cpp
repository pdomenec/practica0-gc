/*
 * File:   Suelo.cpp
 * Author: Javier L�pez Quesada y Pedro Pablo Dom�nech Arellano
 */
#include <gtc/matrix_transform.hpp>  
#include <gtc/type_ptr.hpp>

#include "Suelo.h"

Suelo::Suelo(GLdouble w, GLdouble h, GLuint rw, GLuint rh, Texture *tex) {
	mMesh = Mesh::generaRectanguloTex(w, h, rw, rh);
	mTexture = tex;
	entityType = "Suelo";
}

void Suelo::render(glm::dmat4 const& modelViewMat) const
{
	if (mMesh != nullptr) {
		glm::dmat4 aMat = modelViewMat * mModelMat;  // glm matrix multiplication
		mTexture->bind(GL_REPLACE);
		glPolygonMode(GL_BACK, GL_LINE);
		glEnable(GL_ALPHA_TEST);
		glAlphaFunc(GL_GREATER, 0.0);
		upload(aMat);
		mMesh->render();
		glDisable(GL_ALPHA_TEST);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		mTexture->unbind();
	}
}
