#include "Esfera.h"
#include <iostream>
#include <gtc\type_ptr.hpp>
Esfera::Esfera(GLdouble r, GLint p, GLint n)
{
	//p=9;
	GLdouble angle = glm::radians(180.0 / (p-1));
	//p++;
	GLdouble theta = glm::radians(-90.0);
	std::vector<glm::dvec3> perfil;
	for (int i = 0; i < p; i++)
	{
		//std::cout << glm::degrees(theta) << std::endl;
		GLdouble c = cos(theta);
		GLdouble s = sin(theta);
		GLdouble x = c * r;
		GLdouble y = s * r;
		theta += angle;
		perfil.push_back({ x, y, 0 });
	}
	this->mMesh = MbR::generaIndexMeshByRevolution(n, p, perfil);
}

void Esfera::render(glm::dmat4 const& modelViewMat) const
{
	if (mMesh != nullptr) {
		glm::dmat4 aMat = modelViewMat * mModelMat;  // glm matrix multiplication
		upload(aMat);
		if (material) {
			//setGold();
			material->upload();
		}
		else {
			glEnable(GL_COLOR_MATERIAL);
			glColor3f(color.r, color.g, color.b);
		}
		mMesh->render();
		if (!material) {
			glColor3f(1.0, 1.0, 1.0);
			glDisable(GL_COLOR_MATERIAL);
		}
	}
}

void Esfera::setGold() const{
	glm::fvec3 ambient = { 0.24725, 0.1995, 0.0745 };
	glm::fvec3 diffuse = { 0.75164, 0.60648, 0.22648 };
	glm::fvec3 specular = { 0.628281, 0.555802, 0.366065 };
	glMaterialfv(GL_FRONT, GL_AMBIENT, value_ptr(ambient));
	glMaterialfv(GL_FRONT, GL_DIFFUSE, value_ptr(diffuse));
	glMaterialfv(GL_FRONT, GL_SPECULAR, value_ptr(specular));
	glMaterialf(GL_FRONT, GL_SHININESS, 51.2);
}
