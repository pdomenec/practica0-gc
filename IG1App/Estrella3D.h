/*
 * File:   Estrella3D.h
 * Author: Javier L�pez Quesada y Pedro Pablo Dom�nech Arellano
 */
#ifndef _H_Estrella3D_H_
#define _H_Estrella3D_H_

#include "Entity.h"
class Estrella3D : public Abs_Entity
{
protected:
	GLdouble angleRot;
	glm::dmat4 mTrans;
public:
	explicit Estrella3D(GLdouble re, GLdouble np, GLdouble h);
	virtual void render(glm::dmat4 const& modelViewMat) const;
	virtual void update(glm::dmat4 const& modelViewMat);
};

#endif // _H_Estrella3D_H_
