#pragma once
#include "Entity.h"

#include "IndexMesh.h"

class EntityWithIndexMesh : public Abs_Entity
{
protected:
	IndexMesh* mMesh = nullptr;
public:
	EntityWithIndexMesh() {};
	explicit EntityWithIndexMesh(GLdouble l);
	virtual void render(glm::dmat4 const& modelViewMat) const;
};

