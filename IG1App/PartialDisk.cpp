#include <gtc/matrix_transform.hpp>  
#include <gtc/type_ptr.hpp>

#include "PartialDisk.h"


PartialDisk::PartialDisk(GLdouble iRad, GLdouble oRad, GLdouble startAng, GLdouble sweepAng)
{
	innerRadius = iRad;
	outterRadius = oRad;
	startAngle = startAng;
	sweepAngle = sweepAng;
}

void PartialDisk::render(glm::dmat4 const& modelViewMat) const {
	glm::dmat4 aMat = modelViewMat * mModelMat;
	upload(aMat);
	// Aqu� se puede fijar el color de la esfera as�:
	glEnable(GL_COLOR_MATERIAL);
	glColor3f(color.r, color.g, color.b);
	// Aqu� se puede fijar el modo de dibujar la esfera:
	gluQuadricDrawStyle(q, GLU_FILL);
	gluPartialDisk(q, innerRadius, outterRadius, 50, 50, startAngle, sweepAngle);


	// Aqu� se debe recuperar el color:
	glColor3f(1.0, 1.0, 1.0);
	glDisable(GL_COLOR_MATERIAL);
}