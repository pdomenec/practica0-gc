/*
 * File:   Sierpinski.cpp
 * Author: Javier L�pez Quesada y Pedro Pablo Dom�nech Arellano
 */
#include <gtc/matrix_transform.hpp>  
#include <gtc/type_ptr.hpp>

#include "Sierpinski.h"


Sierpinski::Sierpinski(GLdouble rd, GLuint numP) : Abs_Entity()
{
	mMesh = Mesh::generarSierpinski(rd, numP);
	entityType = "Sierpinski";
}

void Sierpinski::render(glm::dmat4 const& modelViewMat) const
{
	if (mMesh != nullptr) {
		glm::dmat4 aMat = modelViewMat * mModelMat;  // glm matrix multiplication
		glPointSize(2);
		glColor4dv(glm::value_ptr(mColor));
		upload(aMat);
		mMesh->render();
		glPointSize(1);
		glColor4dv(glm::value_ptr(glm::dvec4(1.0,1.0,1.0,1.0)));
	}
}