#pragma once
#include "CompoundEntity.h"
#include "Helices.h"
#include "Sphere.h"
#include <gtc/matrix_transform.hpp>  
#include <gtc/type_ptr.hpp>

class Chasis : public CompoundEntity
{
protected:
	Helices *helices;
	Sphere *bola;
public:
	explicit Chasis();
	void update(glm::dmat4 const& modelViewMat);
};

