/*
 * File:   Scene.cpp
 * Author: Javier L�pez Quesada y Pedro Pablo Dom�nech Arellano
 */
#include "Scene.h"
#include "CheckML.h"
#include <gtc/matrix_transform.hpp>  
#include <gtc/type_ptr.hpp>

using namespace glm;
//-------------------------------------------------------------------------

void Scene::init()
{ 
	setGL();  // OpenGL settings

	// allocate memory and load resources
    // Lights
    // Textures
	

	switch (mId)
	{
	case 1:
		quadricScene1();
		ejesRGB();
		break;
	case 2:
		//anilloCuadradoScene1();
		cuboIndexMesh();
		ejesRGB();
		break;
	case 3:
		//cuboEntity();
		//avionScene();
		//conoMbR();
		//sphereVsEsfera();
		avionSobreEsfera();
		ejesRGB();
		break;
	case 4:
		avionSobreEsferaMaterial();
		ejesRGB();
		break;
	case 5:
		
		ejesRGB();
		break;
	case 6:
		
		ejesRGB();
		break;
	default:
		break;
	}
	
}
//-------------------------------------------------------------------------
void Scene::free() 
{ // release memory and resources   
	

	for (Abs_Entity* el : gObjects)
	{
		delete el;  el = nullptr;
	}
	gObjects.clear();


	for (Texture* tx : gTextures)
	{
		delete tx;  tx = nullptr;
	}
	gTextures.clear();
}
//-------------------------------------------------------------------------
void Scene::setGL() 
{
	// OpenGL basic setting
	glClearColor(0.7, 0.8, 0.9, 1.0);  // background color (alpha=1 -> opaque)
	glEnable(GL_DEPTH_TEST);  // enable Depth test 
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_LIGHTING);
	glEnable(GL_NORMALIZE);

}
//-------------------------------------------------------------------------
void Scene::resetGL() 
{
	glClearColor(.0, .0, .0, .0);  // background color (alpha=1 -> opaque)
	glDisable(GL_DEPTH_TEST);  // disable Depth test 
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);
	glDisable(GL_NORMALIZE);
}
//-------------------------------------------------------------------------

void Scene::render(Camera const& cam) const 
{
	/*sceneDirLight(cam);
	scenePosLight(cam);
	sceneSpotLight(cam);*/
	setLights(cam.viewMat());
	sceneDarkness(cam);
	cam.upload();

	for (Abs_Entity* el : gObjects)
	{
	  el->render(cam.viewMat());
	}
}
//-------------------------------------------------------------------------

void Scene::update(Camera const& cam)
{

	cam.upload();

	for (Abs_Entity* el : gObjects)
	{
		el->update(cam.viewMat());
	}
}

void Scene::setState(int id)
{
	if (mId != id) {
		mId = id;
		free();
		init();
	}
}



void Scene::ejesRGB()
{

	GLdouble rd = 250.0;

	gObjects.push_back(new EjesRGB(rd));

}

void Scene::poligonoRegular()
{

	GLdouble rd = 250.0;

	gObjects.push_back(new EjesRGB(rd));

	Poligono* triangulo = new Poligono(3, rd);
	triangulo->setMColor(glm::dvec4(1.0, 1.0, 0.0, 1.0));
	gObjects.push_back(triangulo);

	Poligono* circulo = new Poligono(360, rd);
	circulo->setMColor(glm::dvec4(1.0, 0.0, 1.0, 1.0));
	gObjects.push_back(circulo);
}

void Scene::trianguloDeSierpinski()
{

	GLdouble rd = 250.0;

	Sierpinski* sierpinski = new Sierpinski(rd, 2000);
	sierpinski->setMColor(glm::dvec4(1.0, 1.0, 0.0, 1.0));
	gObjects.push_back(sierpinski);
}

void Scene::trianguloRGB()
{
	GLdouble rd = 25.0;

	TrianguloRGB* trianguloRGB = new TrianguloRGB(rd);
	gObjects.push_back(trianguloRGB);
}

void Scene::rectangulo()
{

	GLdouble w = 1000.0;
	GLdouble h = 700.0;

	RectanguloRGB* rectanguloRGB = new RectanguloRGB(w, h);
	gObjects.push_back(rectanguloRGB);
}

void Scene::composicionEscena2D()
{
	poligonoRegular();
	trianguloDeSierpinski();
	trianguloRGB();
	rectangulo();
}

void Scene::estrella3DTex()
{

	GLdouble re = 200.0;
	GLdouble np = 8.0;
	GLdouble h = 100.0;

	Texture* texEstrella = new Texture();
	texEstrella->load("../Bmps/baldosaP.bmp", 255); // alpha 255 = opaco, alpha 0 = transparente
	gTextures.push_back(texEstrella);

	Estrella3DTex* estrella3D = new Estrella3DTex(re, np, h, texEstrella);
	estrella3D->setMColor(glm::dvec4(1.0, 1.0, 0.0, 1.0));
	estrella3D->setModelMat(glm::translate(estrella3D->modelMat(), glm::dvec3(-130, 200, -130)) * glm::scale(estrella3D->modelMat(), glm::dvec3(0.25, 0.25, 0.25)));
	gObjects.push_back(estrella3D);
}

void Scene::estrella3D()
{

	GLdouble re = 200.0;
	GLdouble np = 8.0;
	GLdouble h = 100.0;

	Estrella3D* estrella3D = new Estrella3D(re, np, h);
	estrella3D->setMColor(glm::dvec4(1.0, 1.0, 0.0, 1.0));
	gObjects.push_back(estrella3D);
}

void Scene::caja()
{

	GLdouble ld = 150.0;

	Caja* caja = new Caja(ld);
	caja->setMColor(glm::dvec4(1.0, 1.0, 0.0, 1.0));
	gObjects.push_back(caja);
}

void Scene::cajaTex()
{

	GLdouble ld = 150.0;

	Texture* texExt = new Texture();
	Texture* texInt = new Texture();
	texExt->load("../Bmps/container.bmp", 255); // alpha 255 = opaco, alpha 0 = transparente
	texInt->load("../Bmps/papelE.bmp", 255);

	gTextures.push_back(texExt);
	gTextures.push_back(texInt);

	CajaTex* cajaTex = new CajaTex(ld, texExt, texInt);
	cajaTex->setMColor(glm::dvec4(1.0, 1.0, 0.0, 1.0));
	cajaTex->setModelMat(glm::translate(cajaTex->modelMat(), glm::dvec3(-130, 37.51, -130)) * glm::scale(cajaTex->modelMat(), glm::dvec3(0.5, 0.5, 0.5)));
	gObjects.push_back(cajaTex);
}

void Scene::sueloTex()
{
	GLdouble w = 500.0;
	GLdouble h = 500.0;
	GLuint rw = 5;
	GLuint rh = 5;

	Texture* texSuelo = new Texture();
	texSuelo->load("../Bmps/baldosaC.bmp", 255); // alpha 255 = opaco, alpha 0 = transparente
	gTextures.push_back(texSuelo);

	Suelo* suelo = new Suelo(w, h, rw, rh, texSuelo);
	suelo->setModelMat(glm::rotate(glm::dmat4(1.0), glm::radians(270.0), glm::dvec3(1, 0, 0)));
	gObjects.push_back(suelo);
}

void Scene::composicionEscena3D()
{
	cajaTex();
	estrella3DTex();
	sueloTex();
	foto();
	gObjects.push_back(new EjesRGB(200.0));
}

void Scene::foto()
{
	GLdouble wr = 200.0;
	GLdouble hr = 150.0;

	Texture* texFoto = new Texture();
	gTextures.push_back(texFoto);

	Foto* foto = new Foto(wr, hr, texFoto);
	
	gObjects.push_back(foto);
}

void Scene::takePicture()
{

	for (Abs_Entity* obj : gObjects) {
		if (obj->getEntityType() == "Foto") {
			((Foto*)obj)->takePicture();
		}
	}

}

void Scene::cristalTranslucido()
{

	GLdouble ld = 500.0;

	Texture* texture = new Texture();
	texture->load("../Bmps/windowV.bmp", 100); // alpha 100 para hacerlo semi-transparente
	gTextures.push_back(texture);

	CajaCristal* cajaTex = new CajaCristal(ld, texture, texture);
	cajaTex->setMColor(glm::dvec4(1.0, 0.0, 0.0, 0.25));
	cajaTex->setRenderFondo(false);
	cajaTex->setModelMat(glm::translate(cajaTex->modelMat(), glm::dvec3(0.0, 125.0, 0.0)) * glm::scale(cajaTex->modelMat(), glm::dvec3(1.0, 0.5, 1.0)));
	gObjects.push_back(cajaTex);
}

void Scene::composicionEscena3DCristalera()
{
	
	
	cajaTex();
	estrella3DTex();
	sueloTex();
	planta();
	foto();
	cristalTranslucido();

}

void Scene::planta()
{
	GLdouble w = 100.0;
	GLdouble h = 100.0;

	Texture* tex = new Texture();
	glm::u8vec3 color = glm::u8vec3(0.0, 0.0, 0.0);
	tex->load("../Bmps/grass.bmp", color, 255);
	tex->setWrap(GL_CLAMP);
	gTextures.push_back(tex);

	int numR = 4; //numero de rectangulos cruzados en el eje Y que queremos para mostrar la planta
	GLdouble angle = glm::radians(0.0); 
	GLdouble aumAngle = glm::radians(180.0/ numR); //angulo que hay que aumentar para el siguiente rectangulo cruzado (180 porque se cruzan y aparecen por el otro lado)

	//Generar los numR rect�ngulos que se cruzan.
	//Al usar la clase Suelo que no renderiza por la cara BACK generamos 2*numR rectangulos cruzados, para que se solape la cara BACK de uno con la FRONT de otro
	for (int i = 0; i < 2 * numR; i++) {
		Suelo* rectangulo = new Suelo(w, h, 1, 1, tex);
		rectangulo->setModelMat(glm::translate(rectangulo->modelMat(), glm::dvec3(130.0, 51.0, -130.0)) * glm::rotate(glm::dmat4(1.0), angle, glm::dvec3(0, 1, 0)));

		gObjects.push_back(rectangulo);
		angle += aumAngle;
	}
}

void Scene::quadricScene1()
{

	Sphere* esfera = new Sphere(100.0);
	glm::fvec3 colorCarne = glm::fvec3(250.0 / 255.0, 165.0 / 255.0, 114.0 / 255.0);
	esfera->setColor(colorCarne);
	gObjects.push_back(esfera);

	Cylinder* conoOjoAzul = new Cylinder(10.0, 0.0, 20.0);
	glm::fvec3 colorAzul = glm::fvec3(0.0, 0.0, 1.0);
	conoOjoAzul->setColor(colorAzul);
	conoOjoAzul->setModelMat(glm::translate(conoOjoAzul->modelMat(), glm::dvec3(30.0, 40.0, 80.0)));
	gObjects.push_back(conoOjoAzul);

	Cylinder* conoOjoRosa = new Cylinder(10.0, 0.0, 20.0);
	glm::fvec3 colorRosa = glm::fvec3(1.0, 0.0, 0.5);
	conoOjoRosa->setColor(colorRosa);
	conoOjoRosa->setModelMat(glm::translate(conoOjoRosa->modelMat(), glm::dvec3(-30.0, 40.0, 80.0)));
	gObjects.push_back(conoOjoRosa);

	Disk* discoAureola = new Disk(50.0, 105.0);
	glm::fvec3 colorRojo = glm::fvec3(1.0, 0.0, 0.0);
	discoAureola->setColor(colorRojo);
	discoAureola->setModelMat(glm::translate(discoAureola->modelMat(), glm::dvec3(0.0, 80.0, 0.0)) * glm::rotate(glm::dmat4(1.0), glm::radians(-85.0), glm::dvec3(1, 0, 0)));
	gObjects.push_back(discoAureola);

	PartialDisk* partialDiskSonrisa = new PartialDisk(60.0, 70.0, 90.0, 180.0);
	glm::fvec3 colorVerde = glm::fvec3(0.0, 1.0, 0.0);
	partialDiskSonrisa->setColor(colorVerde);
	partialDiskSonrisa->setModelMat(glm::translate(partialDiskSonrisa->modelMat(), glm::dvec3(0.0, 0.0, 77.0)));
	gObjects.push_back(partialDiskSonrisa);

}

void Scene::sceneDirLight(Camera const& cam) const
{
	if (this->activeDirLight) {
		glEnable(GL_LIGHT0);
		glm::fvec4 posDir = { 1, 1, 1, 0 };
		glMatrixMode(GL_MODELVIEW);
		glLoadMatrixd(value_ptr(cam.viewMat()));
		glLightfv(GL_LIGHT0, GL_POSITION, value_ptr(posDir));
		glm::fvec4 ambient = { 0, 0, 0, 1 };
		glm::fvec4 diffuse = { 1, 1, 1, 1 };
		glm::fvec4 specular = { 0.5, 0.5, 0.5, 1 };
		glLightfv(GL_LIGHT0, GL_AMBIENT, value_ptr(ambient));
		glLightfv(GL_LIGHT0, GL_DIFFUSE, value_ptr(diffuse));
		glLightfv(GL_LIGHT0, GL_SPECULAR, value_ptr(specular));
	}
	else {
		glDisable(GL_LIGHT0);
	}
}

void Scene::scenePosLight(Camera const& cam) const
{
	if (this->activePosLight) {
		glEnable(GL_LIGHT1);
		glm::fvec4 posDir = { 300, 300, 0, 1 };
		glMatrixMode(GL_MODELVIEW);
		glLoadMatrixd(value_ptr(cam.viewMat()));
		glLightfv(GL_LIGHT1, GL_POSITION, value_ptr(posDir));
		glm::fvec4 ambient = { 0, 0, 0, 1 };
		glm::fvec4 diffuse = { 0.0, 1, 0.0, 1 };
		glm::fvec4 specular = { 0.5, 0.5, 0.5, 1 };
		glLightfv(GL_LIGHT1, GL_AMBIENT, value_ptr(ambient));
		glLightfv(GL_LIGHT1, GL_DIFFUSE, value_ptr(diffuse));
		glLightfv(GL_LIGHT1, GL_SPECULAR, value_ptr(specular));
	}
	else {
		glDisable(GL_LIGHT1);
	}
}

void Scene::sceneSpotLight(Camera const& cam) const
{
	if (this->activeSpotLight) {
		glEnable(GL_LIGHT2);
		glMatrixMode(GL_MODELVIEW);
		glLoadMatrixd(value_ptr(cam.viewMat()));
		glLightf(GL_LIGHT2, GL_SPOT_CUTOFF, 45.0);
		glLightf(GL_LIGHT2, GL_SPOT_EXPONENT, 4.0);
		glm::fvec3 dir = { 0, -1, -1};
		glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, value_ptr(dir));
		glm::fvec4 posDir = { 0, 300, 300, 1 };
		glLightfv(GL_LIGHT2, GL_POSITION, value_ptr(posDir));

		glm::fvec4 ambient = { 0, 0, 0, 1 };
		glm::fvec4 diffuse = { 0.0, 1, 0.0, 1 };
		glm::fvec4 specular = { 0.5, 0.5, 0.5, 1 };
		glLightfv(GL_LIGHT2, GL_AMBIENT, value_ptr(ambient));
		glLightfv(GL_LIGHT2, GL_DIFFUSE, value_ptr(diffuse));
		glLightfv(GL_LIGHT2, GL_SPECULAR, value_ptr(specular));
	}
	else {
		glDisable(GL_LIGHT2);
	}
}

void Scene::sceneDarkness(Camera const& cam) const
{
	if (activeDarkness) {
		GLfloat amb[] = { 0.0, 0.0, 0.0, 1.0 };
		glLightModelfv(GL_LIGHT_MODEL_AMBIENT, amb);
	}
	else {
		GLfloat amb[] = { 0.2, 0.2, 0.2, 1.0 };
		glLightModelfv(GL_LIGHT_MODEL_AMBIENT, amb);
	}
}

void Scene::setLights(glm::dmat4 const& modelViewMat) const
{
	if (this->activeDirLight) {
		this->directionalLight->enable();
		this->directionalLight->upload(modelViewMat);
	}
	else {
		this->directionalLight->disable();
	}

	if (this->activeDirLight2) {
		this->directionalLight2->enable();
		this->directionalLight2->upload(modelViewMat);
	}
	else {
		this->directionalLight2->disable();
	}

	if (this->activePosLight) {
		this->positionalLight->enable();
		this->positionalLight->upload(modelViewMat);
	}
	else {
		this->positionalLight->disable();
	}

	if (this->activeSpotLight) {
		this->spotSceneLight->enable();
		this->spotSceneLight->upload(modelViewMat);
	}
	else {
		this->spotSceneLight->disable();
	}

	if (this->activeMinerLight) {
		this->minerLight->enable();
		this->minerLight->upload(dmat4(1.0));
	}
	else {
		this->minerLight->disable();
	}

	Avion* avion = this->getAvion();
	if (avion) {
		if (activeFocoAvion) {
			avion->encenderFoco(modelViewMat);
		}
		else {
			avion->apagarFoco();
		}
	}
	
}

Avion* Scene::getAvion() const{
	for (Abs_Entity* el : gObjects)
	{
		if (el->getEntityType() == "Avion") {
			return (Avion*)el;
		}
	}
	return nullptr;
}

void Scene::anilloCuadradoScene1()
{
	AnilloCuadrado *anilloCuadrado = new AnilloCuadrado();
	gObjects.push_back(anilloCuadrado);
}

void Scene::cuboIndexMesh()
{
	EntityWithIndexMesh *cubo = new EntityWithIndexMesh(100.0);
	gObjects.push_back(cubo);
}

void Scene::cuboEntity()
{
	Cubo* cubo = new Cubo(100.0, { 1, 0, 0, 1 });
	cubo->setMColor(glm::dvec4(0, 1, 0, 1));
	gObjects.push_back(cubo);
}

void Scene::avionScene()
{
	Avion* avion = new Avion();
	gObjects.push_back(avion);
}

void Scene::conoMbR()
{
	Cono* cono = new Cono(100.0, 50.0, 360);
	cono->setMColor({ 0, 0, 1, 1 });
	cono->generateVColors();
	gObjects.push_back(cono);
}

void Scene::esferaMbR()
{
	Esfera* esfera = new Esfera(150.0, 50, 50);
	esfera->setMColor({ 0, 0, 1, 1 });
	gObjects.push_back(esfera);
}

void Scene::sphereVsEsfera()
{
	Sphere* sphere = new Sphere(50.0);
	sphere->setColor({ 0, 0, 1 });
	sphere->setModelMat(
		glm::translate(sphere->modelMat(), glm::dvec3(100.0, 0, 0))
	);
	gObjects.push_back(sphere);

	Esfera* esfera = new Esfera(50.0, 50, 50);
	esfera->setMColor({ 0, 0, 1, 1 });
	esfera->setModelMat(
		glm::translate(esfera->modelMat(), glm::dvec3(0, 0, 100.0))
	);
	gObjects.push_back(esfera);
}

void Scene::avionSobreEsfera()
{
	Esfera* esfera = new Esfera(200.0, 200, 200);
	esfera->setColor({ 0, 1, 0.9});
	gObjects.push_back(esfera);

	Avion* avion = new Avion();
	avion->setModelMat(
		glm::scale(avion->modelMat(), glm::dvec3(0.3, 0.3, 0.3)) 
	);

	gObjects.push_back(avion);
}

void Scene::avionSobreEsferaMaterial()
{
	Esfera* esfera = new Esfera(200.0, 200, 200);
	esfera->setColor({ 0, 1, 0.9 });
	gObjects.push_back(esfera);

	Material* materialOro = new Material();
	materialOro->setGold();
	esfera->setMaterial(materialOro);

	Avion* avion = new Avion();
	avion->setModelMat(
		glm::scale(avion->modelMat(), glm::dvec3(0.3, 0.3, 0.3))
	);

	gObjects.push_back(avion);
}

void Scene::setActiveDirLigth(bool show)
{
	this->activeDirLight = show;
}

void Scene::setActiveDirLigth2(bool show)
{
	this->activeDirLight2 = show;
}

void Scene::setActivePosLigth(bool show)
{
	this->activePosLight = show;
}

void Scene::setActiveSpotLigth(bool show)
{
	this->activeSpotLight = show;
}

void Scene::setActiveFocoAvion(bool show)
{
	this->activeFocoAvion = show;
}

void Scene::setActiveMinerLight(bool show)
{
	this->activeMinerLight = show;
}

void Scene::setActiveDarkness(bool show)
{
	this->activeDarkness = show;
}

void Scene::move(Camera const& cam) {
	cam.upload();

	Avion* avion = this->getAvion();
	if(avion)
		avion->update(cam.viewMat());
}