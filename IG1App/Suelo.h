/*
 * File:   Suelo.h
 * Author: Javier L�pez Quesada y Pedro Pablo Dom�nech Arellano
 */
#ifndef _H_Suelo_H_
#define _H_Suelo_H_

#include "Entity.h"
class Suelo : public Abs_Entity
{
public:
    explicit Suelo(GLdouble, GLdouble, GLuint, GLuint, Texture *);
    virtual void render(glm::dmat4 const& modelViewMat) const;
};

#endif // _H_Suelo_H_