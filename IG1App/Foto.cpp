/*
 * File:   Foto.cpp
 * Author: Javier L�pez Quesada y Pedro Pablo Dom�nech Arellano
 */
#include <gtc/matrix_transform.hpp>  
#include <gtc/type_ptr.hpp>
#include <ctime>

#include "Foto.h"

Foto::Foto(GLuint wr, GLuint hr, Texture* tex) {
	mMesh = Mesh::generaRectanguloTex(wr, hr, 1, 1);
	//mTrans = glm::rotate(glm::translate(glm::dmat4(1.0), glm::dvec3(100.0, 1.0, 100.0)), glm::radians(270.0), glm::dvec3(1, 0, 0));
	mTrans = glm::rotate(glm::translate(glm::dmat4(1.0), glm::dvec3(0.0, 1.0, 0.0)), glm::radians(270.0), glm::dvec3(1, 0, 0));
	widthRectangulo = wr;
	heightRectangulo = hr;
	mTexture = tex;
	entityType = "Foto";
}

void Foto::render(glm::dmat4 const& modelViewMat) const {
	if (mMesh != nullptr) {
		glm::dmat4 aMat = modelViewMat * mModelMat * mTrans;  // glm matrix multiplication
		mTexture->loadColorBuffer();
		mTexture->bind(GL_TEXTURE_2D);
		glPolygonMode(GL_FRONT, GL_FILL);
		upload(aMat);
		mMesh->render();
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		mTexture->unbind();

	}
}

void Foto::update(glm::dmat4 const& modelViewMat) {
	/*mTexture->loadColorBuffer(widthPantalla, heightPantalla);
	mTexture->bind(GL_TEXTURE_2D);
	render(modelViewMat);
	mTexture->unbind();*/
}

void Foto::takePicture() {
	std::time_t t = std::time(0);
	mTexture->save("../capturas/" + std::to_string(t) + "-pictureScene.bmp");
}