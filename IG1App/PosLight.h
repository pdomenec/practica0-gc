#pragma once
#include "Light.h"
class PosLight : public Light {
protected:
    // Factores de atenuación
    GLfloat kc = 1, kl = 0, kq = 0;
public:
    virtual void upload(glm::dmat4 const& modelViewMat) const;
    void setPosDir(glm::fvec3 dir);
    void setAtte(GLfloat _kc, GLfloat _kl, GLfloat _kq);
};

