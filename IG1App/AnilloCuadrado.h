#pragma once
#include "Entity.h"

#include "IndexMesh.h"
class AnilloCuadrado : public Abs_Entity
{
protected:
	IndexMesh* mMesh = nullptr;
public:
	explicit AnilloCuadrado();
	virtual void render(glm::dmat4 const& modelViewMat) const;
};

