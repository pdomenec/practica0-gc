/*
 * File:   CajaTex.cpp
 * Author: Javier L�pez Quesada y Pedro Pablo Dom�nech Arellano
 */
#include <gtc/matrix_transform.hpp>  
#include <gtc/type_ptr.hpp>

#include "CajaTex.h"

CajaTex::CajaTex(GLdouble _ld, Texture* _texturaExt, Texture* _texturaInt) {
	ld = _ld;
	mMesh = Mesh::generaCajaTexCor(ld);                   // Se genera la caja con las coordenadas de textura
	meshFondo = Mesh::generaRectanguloTex(ld, ld, 1, 1);  // Se genera un rect�ngulo con las coordenadas de textura
	mTrans = glm::dmat4(1.0);

	//Asignamos la textura exterior y la textura interior
	mTexture = _texturaExt;
	texturaInt = _texturaInt;

	entityType = "CajaTex";
	renderFondo = true;
}

/**
  * @brief Transfiere la matriz de modelado a la GPU
  * @param modelViewMat matriz de modelado
  */
void CajaTex::render(glm::dmat4 const& modelViewMat) const {
	if (mMesh != nullptr) {
		glm::dmat4 aMat = modelViewMat * mTrans * mModelMat;  // glm matrix multiplication
		upload(aMat);
		glEnable(GL_BLEND);                                   // Se habilita el blending para crear efectos de transparencia
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_CULL_FACE);								  // Se habilita la elecci�n de caras (front o back)
			mTexture->bind(GL_TEXTURE_2D);
			glCullFace(GL_FRONT);
			mMesh->render();
			mTexture->unbind();
		
			texturaInt->bind(GL_TEXTURE_2D);
			glCullFace(GL_BACK);
			mMesh->render();
			texturaInt->unbind();
		glDisable(GL_CULL_FACE);


		glm::dmat4 mTransSuelo = glm::rotate(glm::dmat4(1.0), glm::radians(90.0), glm::dvec3(1, 0, 0));
		mTransSuelo = glm::translate(mTransSuelo, glm::dvec3(0, 0, ld / 2.0));
		glm::dmat4 aMatSuelo = modelViewMat * mTrans * mModelMat * mTransSuelo;  // glm matrix multiplication
		if (renderFondo) {
			glm::dmat4 mTransSuelo = glm::rotate(glm::dmat4(1.0), glm::radians(90.0), glm::dvec3(1, 0, 0));
			mTransSuelo = glm::translate(mTransSuelo, glm::dvec3(0, 0, ld / 2.0));
			glm::dmat4 aMatSuelo = modelViewMat * mTrans * mModelMat * mTransSuelo;  // glm matrix multiplication
			upload(aMatSuelo); //Hay que actualizar la matriz de modelado para que se ponga la textura en las coordenadas correctas

			glEnable(GL_CULL_FACE);
				mTexture->bind(GL_TEXTURE_2D);
				glCullFace(GL_BACK);
				meshFondo->render();
				mTexture->unbind();

				texturaInt->bind(GL_TEXTURE_2D);
				glCullFace(GL_FRONT);
				meshFondo->render();
				texturaInt->unbind();
			glDisable(GL_CULL_FACE);
		}

		upload(aMat);
		mMesh->render();

		if (renderFondo) {
			upload(aMatSuelo);
			meshFondo->render();
		}
		glDisable(GL_BLEND);
		
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
}
