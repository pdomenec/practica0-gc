#include "EntityWithIndexMesh.h"
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>

EntityWithIndexMesh::EntityWithIndexMesh(GLdouble l) {
	mMesh = IndexMesh::generaIndexCuboConTapas(l, { 1, 0, 0, 1 });
	entityType = "EntityWithIndexMesh";
}

void EntityWithIndexMesh::render(glm::dmat4 const& modelViewMat) const
{
	if (mMesh != nullptr) {
		glm::dmat4 aMat = modelViewMat * mModelMat;  // glm matrix multiplication
		upload(aMat);
		glEnable(GL_COLOR_MATERIAL);
		mMesh->render();
		glDisable(GL_COLOR_MATERIAL);
	}
}
