#pragma once
#include "QuadricEntity.h"
class Cylinder : public QuadricEntity
{
public:
	Cylinder(GLdouble bRad, GLdouble tRad, GLdouble h);
	void render(glm::dmat4 const& modelViewMat) const;
protected:
	GLdouble baseRadius;
	GLdouble topRadius;
	GLdouble height;
};
