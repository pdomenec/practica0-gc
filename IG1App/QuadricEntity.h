#pragma once
#include "Entity.h"
class QuadricEntity : public Abs_Entity
{
public:
	QuadricEntity();
	~QuadricEntity() { gluDeleteQuadric(q); };

	void setColor(glm::fvec3 c) { color = c; };
protected:
	GLUquadricObj* q;
	glm::fvec3 color = glm::fvec3(-1, -1, -1);

};

