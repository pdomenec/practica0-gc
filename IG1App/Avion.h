#pragma once
#include "CompoundEntity.h"
#include "Chasis.h"
#include "Cubo.h"
#include <gtc/matrix_transform.hpp>  
#include <gtc/type_ptr.hpp>
#include "SpotLight.h"

class Avion : public CompoundEntity
{
protected:
	Chasis *chasis;
	Cubo *alas;
	SpotLight* focoVientre;
	GLdouble angleRot;
	GLdouble angleRotFoco;
	GLdouble angleTrans;
	glm::dmat4 mTrans;
	glm::dmat4 initialMTrans;

public:
	explicit Avion();
	~Avion() {
		delete focoVientre;
		focoVientre = nullptr;
	}
	void render(glm::dmat4 const& modelViewMat) const;
	void encenderFoco(glm::dmat4 const& modelViewMat);
	void apagarFoco();
	void update(glm::dmat4 const& modelViewMat);
};

