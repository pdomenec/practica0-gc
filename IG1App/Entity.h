/*
 * File:   Entity.h
 * Author: Javier López Quesada y Pedro Pablo Doménech Arellano
 */
//#pragma once
#ifndef _H_Entities_H_
#define _H_Entities_H_

#include <GL/freeglut.h>
#include <glm.hpp>
#include <string>

#include "Mesh.h"
#include "Texture.h"

/**
 * @class Entity
 * @brief Clase abstracta encargada de la creación de entidades
 */
class Abs_Entity 
{
public:

	/**
	* @brief Constructor por defecto de Entity
	*/
	Abs_Entity() : mModelMat(1.0), mColor(1), mTexture(nullptr) { //Por defecto la matriz identidad de 4x4
		
	};
	
	/**
	* @brief Destructor de Entity
	*/
	virtual ~Abs_Entity() { 
		delete mMesh; 
		mMesh = nullptr;
	};

	Abs_Entity(const Abs_Entity& e) = delete;  // No tiene constructor de copia
	Abs_Entity& operator=(const Abs_Entity& e) = delete;  // No tiene operador de asignación

	virtual void render(glm::dmat4 const& modelViewMat) const = 0;  // Método abstracto para el renderizado de entidades
	virtual void update(glm::dmat4 const& modelViewMat) {};
	/**
	* @brief Devuelve la matriz de modelado
	* @return matriz de modelado
	*/
	glm::dmat4 const& modelMat() const { 
		return mModelMat;
	};
	
	/**
	* @brief Modifica la matriz de modelado
	* @param aMat nueva matriz de modelado
	*/
	void setModelMat(glm::dmat4 const& aMat) { 
		mModelMat = aMat;
	}; 
	
	/**
	* @brief Modifica el color
	* @param new_mColor nuevo color
	*/
	void setMColor(glm::dvec4 new_mColor) { 
		mColor = new_mColor;
	}

	/**
	* @brief Modifica la textura de la entidad
	* @param tex nueva textura
	*/
	void setTexture(Texture* tex) {
		mTexture = tex;
	}

	/**
	* @brief Devuelve el nombre de la entidad que hereda de Abs_Entity 
	*/
	std::string getEntityType() {
		return entityType;
	}
	
protected:
	Mesh *mMesh = nullptr;   // Malla
	glm::dmat4 mModelMat;    // Matriz de modelado
	Texture* mTexture;		// Textura de la entidad
	glm::dvec4 mColor;		 // Color de la malla
	std::string entityType;		// Nombre de la entidad que hereda de Abs_Entity
	
	virtual void upload(glm::dmat4 const& mModelViewMat) const; // Transfiere la matriz de modelado a la GPU
};

#endif //_H_Entities_H_