/*
 * File:   Estrella3DTex.cpp
 * Author: Javier L�pez Quesada y Pedro Pablo Dom�nech Arellano
 */
#include <gtc/matrix_transform.hpp>  
#include <gtc/type_ptr.hpp>

#include "Estrella3DTex.h"


Estrella3DTex::Estrella3DTex(GLdouble re, GLdouble np, GLdouble h, Texture *texEstrella) {
	mMesh = Mesh::generaEstrellaTexCor(re, np, h);
	mTrans = glm::dmat4(1.0);
	angleRot = glm::radians(10.0);
	mTexture = texEstrella;
	entityType = "Estrella3DTex";
}

void Estrella3DTex::render(glm::dmat4 const& modelViewMat) const {
	if (mMesh != nullptr) {
		glm::dmat4 aMat = modelViewMat * mModelMat * mTrans;  // glm matrix multiplication
		glm::dmat4 aMat2 = aMat * (glm::rotate(glm::dmat4(1.0), glm::radians(180.0), glm::dvec3(0,1,0)));  // glm matrix multiplication
		
		mTexture->bind(GL_REPLACE);

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		upload(aMat);
		mMesh->render();
		upload(aMat2);
		mMesh->render();
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		mTexture->unbind();
	}
}
void Estrella3DTex::update(glm::dmat4 const& modelViewMat) {
	glm::dmat4 giroZ = glm::rotate(glm::dmat4(1.0), angleRot, glm::dvec3(0, 0, 1));
	glm::dmat4 giroY = glm::rotate(glm::dmat4(1.0), angleRot, glm::dvec3(0, 1, 0));
	angleRot -= glm::radians(3.0);

	mTrans = giroY * giroZ;

	render(modelViewMat);
}