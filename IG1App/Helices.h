#pragma once
#include "CompoundEntity.h"
#include "Cylinder.h"
#include <gtc/matrix_transform.hpp>  
#include <gtc/type_ptr.hpp>

class Helices : public CompoundEntity
{
protected:
	Cylinder *helice1;
	Cylinder *helice2;
	GLdouble angleRot;
	glm::dmat4 mTrans;
public:
	explicit Helices();
	void update(glm::dmat4 const& modelViewMat);
	void render(glm::dmat4 const& modelViewMat) const;
};

