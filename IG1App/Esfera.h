#pragma once
#include "EntityWithMaterial.h"
#include "MbR.h"
#include <vector>

class Esfera : public EntityWithMaterial
{
protected:
	glm::dvec3 color = { 0, 0, 1 };
public:
	explicit Esfera(GLdouble r, GLint p, GLint n);
	//void generateVColors();
	void render(glm::dmat4 const& modelViewMat) const;
	void setGold() const;
	void setColor(glm::dvec3 c) { color = c; }
};

