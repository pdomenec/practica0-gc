/*
 * File:   TrianguloRGB.cpp
 * Author: Javier L�pez Quesada y Pedro Pablo Dom�nech Arellano
 */
#include <gtc/matrix_transform.hpp>  
#include <gtc/type_ptr.hpp>

#include "TrianguloRGB.h"


TrianguloRGB::TrianguloRGB(GLdouble rd) {
	mMesh = Mesh::generaTrianguloRGB(rd);
	angleRot = glm::radians(0.0);
	angleTrans = glm::radians(0.0);
	radio = 250.0;
	mTrans = glm::dmat4(1.0);
	entityType = "TrianguloRGB";
}

void TrianguloRGB::update(glm::dmat4 const& modelViewMat) {
	glm::dvec3 translacion = glm::dvec3(radio * glm::cos(angleTrans), radio * glm::sin(angleTrans), 0.0);
	angleTrans += glm::radians(5.0);
	glm::dmat4 newmTrans = glm::translate(glm::dmat4(1.0), translacion);
	newmTrans = glm::rotate(newmTrans, angleRot, glm::dvec3(0, 0, 1));
	angleRot -= glm::radians(25.0);

	mTrans = newmTrans;

	render(modelViewMat);
}

void TrianguloRGB::render(glm::dmat4 const& modelViewMat) const
{
	if (mMesh != nullptr) {
		glm::dmat4 aMat = modelViewMat * mTrans * mModelMat;  // glm matrix multiplication
		glPolygonMode(GL_BACK, GL_LINE);
		upload(aMat);
		mMesh->render();
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
}
