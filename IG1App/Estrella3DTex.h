/*
 * File:   Estrella3DTex.h
 * Author: Javier L�pez Quesada y Pedro Pablo Dom�nech Arellano
 */
#ifndef _H_Estrella3DTex_H_
#define _H_Estrella3DTex_H_

#include "Entity.h"
class Estrella3DTex : public Abs_Entity
{
protected:
	GLdouble angleRot;
	glm::dmat4 mTrans;
public:
	explicit Estrella3DTex(GLdouble re, GLdouble np, GLdouble h, Texture* texEstrella);
	virtual void render(glm::dmat4 const& modelViewMat) const;
	virtual void update(glm::dmat4 const& modelViewMat);
	glm::dmat4 getMTrans() { return mTrans; };
};

#endif // _H_Estrella3DTex_H_
