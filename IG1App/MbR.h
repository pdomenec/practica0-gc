#pragma once
#include "IndexMesh.h"
#include <vector>
class MbR : public IndexMesh
{
protected:
	int numMuestras; // numero de muestras
	int numPuntosPerfil; // numero de puntos del perfil
	std::vector<glm::dvec3> perfil; // array de vertices que define el perfil

public:
	MbR(int numMuestras, int numPuntosPerfil, std::vector<glm::dvec3> perfil);

	static MbR* generaIndexMeshByRevolution(int numMuestras, int numPuntosPerfil, std::vector<glm::dvec3> perfil);
};

